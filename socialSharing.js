gorilla.socialSharingData = 
{
	fb:
	{
		home_fb:function(){
			return "page1"
		},				
		products_fb:function(){
			return "page2"
		},		
		fun_fb:function(){
			return "page26"
		},		
		news_fb:function(){
			return "page18"
		},		
		faq_fb:function(){
			return "page41"
		},		
		productsfull_fb:function(){
			return "page3"
		},		
		specificnews_fb:function(){
			return "page19"
		},				
		over_fb:function(){
			//Innovating
			return "page21"
		},		
		chara_fb:function(){
			//Innovating
			return "page22"
		},		
		custo_fb:function(){
			//Innovating
			return "page23"
		},		
		appli_fb:function(){
			//Innovating
			return "page24"
		},		
		lite_fb:function(){
			//Innovating
			return "page25"
		},				
		victor_fb:function(){
			//Videos
			if (selectedVideoToShare === 0)//channel surfing 
            {             	                               
                veltijs.pushCustomEventAnalytics('victor1_fb');   
                return "page33"
            }
            else if (selectedVideoToShare === 1)//cooking up tomorrow's kitchen
            {             	              
                veltijs.pushCustomEventAnalytics('victor2_fb');      
                return "page34"
            }
            else if (selectedVideoToShare === 2)//king of the office
            {             	               
                veltijs.pushCustomEventAnalytics('victor3_fb');   
                return "page35"
            }
		},	
		demos_fb:function(){
			//Videos
			if (selectedVideoToShare === 0)// How Corning Tests Gorilla Glass
            {                                             
                veltijs.pushCustomEventAnalytics('demos1_fb');    
                return "page51"
            }
		},		
		saying_fb:function(){
			//Videos
			if (selectedVideoToShare === 0)//gorilla tough 
            {                                       
                veltijs.pushCustomEventAnalytics('saying1_fb');          
                return "page38"
            }
            else if (selectedVideoToShare === 1)//Microsoft Puts Corning Gorilla Glass 2 to the Test
            {                                              
                veltijs.pushCustomEventAnalytics('saying2_fb');    
                return "page50"
            }
		},		
		brought_fb:function(){
			//Videos
			if (selectedVideoToShare === 0)//a day made of
            {                                                 
                veltijs.pushCustomEventAnalytics('brought1_fb');  
                return "page39"
            }
            else if (selectedVideoToShare === 1)//Corning Gorilla Glass for Seamless Elegant Design
            {                          	           
            	veltijs.pushCustomEventAnalytics('brought2_fb');   
            	return "page49"
            }
            else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
            {                            
                veltijs.pushCustomEventAnalytics('brought3_fb'); 
                return "page52"
            }
		},				
		wallpaper_fb:function(){
			//Wallpapers
			if (currentId == 'coverflow0pic1' )//fatsa
            {               
                veltijs.pushCustomEventAnalytics('coverflow0pic1_fb');
                return "page31"
            }
			else if(currentId == 'coverflow0pic2')//filaei kato
            {               
                veltijs.pushCustomEventAnalytics('coverflow0pic2_fb');
                return "page29"
            }
			else if(currentId == 'coverflow0pic3')//kornizes
            {                             
                veltijs.pushCustomEventAnalytics('coverflow0pic3_fb');
                return "page30"
            }
			else if(currentId == 'coverflow0pic4')//bananes
            {               
                veltijs.pushCustomEventAnalytics('coverflow0pic4_fb');
                return "page28"
            }
			else if (currentId == 'coverflow0pic5')// Fossey
            {
                veltijs.pushCustomEventAnalytics('coverflow0pic5_fb');
                return "page48"
            }
		},		
		//Products
		acer_fb:function(){
			return "page4"
		},		
		dell_fb:function(){
			return "page6"
		},		
		htc_fb:function(){
			return "page7"
		},		
		kyo_fb:function(){
			return "page8"
		},		
		asus_fb:function(){
			return "page5"
		},		
		lg_fb:function(){
			return "page10"
		},		
		moto_fb:function(){
			return "page11"
		},		
		moti_fb:function(){
			return "page12"
		},		
		nec_fb:function(){
			return "page13"
		},		
		nokia_fb:function(){
			return "page14"
		},		
		sam_fb:function(){
			return "page15"
		},		
		sk_fb:function(){
			return "page16"
		},		
		sony_fb:function(){
			return "page17"
		},		
		lenovo_fb:function(){
			return "page9"
		},		
		hyundai_fb:function(){
			return "page40"
		},		
		praise_fb:function(){
			return "page44"
		},		
		allpraise_fb:function(){
			return "page44"
		},		
		sur40_fb:function(){
			return "page46"
		},		
		hp_fb:function(){
			return "page45"
		},		
		sonim_fb:function(){
			return "page47"
		},		
		lumigon_fb:function(){
			return "page53"
		}			
	}
}
       
 