var gorilla = {};

gorilla._init_ = function()
{
		
	if(location.hash=="")
	{
		location.hash="home_page";
		//veltijs.locationchangeView('home_page');
	}
	else
	{
		var pageToGo = location.hash.replace('#','');	
		veltijs.locationchangeView(pageToGo);
	}
	
	if(veltijs.imagesPreloaded)
	{
		document.getElementById('velti_box').style.display ="block";
		document.getElementById('loader').style.display ="none";
	}
	
	veltijs.deviceOrientation = veltijs.getOrientation();
	
	gorilla.preloadFunWallpapers();
	console.log("finishing body.onload**************************************************");
	
}


gorilla.hovereffect = function(el,state,btn)
{
	//alert(0);
	if (window.navigator.userAgent.toLowerCase().search('blackberry') == -1){
		if(btn){
			(state)?el.style.color="white":el.style.color="#009ED3";
		}
		else{
		(state)?el.style.background="#cccccc":el.style.background="none";}
	} 
}



gorilla.orientationChangeListener = function(o)
{
	var str = location.hash;
	var page = str.replace("#","");	
	gorilla.setVideoOrientation(o,"");
}

gorilla.setServices = (function()
{
 			  
  
    // set services urls for staging and live envs 
    var objServices = [{
        press_release_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/press_release',
        news_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/news',
        events_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/event',
        showall_press_release_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/press_release',
        showall_news_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/news',
        showall_events_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/event',
        sendfile_viaemail_service:'http://mgage-us-staging.velti.com/cms/davinci/page/testStav/@dev/page1',
        sendfile_viaemail_host:'http://mgage-us-staging.velti.com/cms/plugins/com/velti/SendToFriendViaEmail/backEnd/SendEmailService',
        facebook_url:'http://gorilla-stag.v4en.com/fb/share/share.jsp?p=',
        faq_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/faqs',
        media_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/media',
        consumers_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/consumers',
        submit_service:'http://gorilla-stag.v4en.com/rssReader/gorilla/comment',
        sms_service:'http://10.64.102.210:8080/message-toolkit/messagerequests',
        google_analytics_id:'UA-33212028-1',
        visualize_analytics_url:'visualize-greece.velti.com',
    	visualize_analytics_siteId:'21',
    	visualize_analytics_EventssiteId:'22'
    },
    {
        press_release_service:'http://gorilla.v4en.com/rssReader/gorilla/press_release',
        news_service:'http://gorilla.v4en.com/rssReader/gorilla/news',
        events_service:'http://gorilla.v4en.com/rssReader/gorilla/event',
        showall_press_release_service:'http://gorilla.v4en.com/rssReader/gorilla/press_release',
        showall_news_service:'http://gorilla.v4en.com/rssReader/gorilla/news',
        showall_events_service:'http://gorilla.v4en.com/rssReader/gorilla/event',
        sendfile_viaemail_service:'http://mgage-us.velti.com/cms/davinci/page/testStav/@dev/page1',
        sendfile_viaemail_host:'http://mgage-us.velti.com/cms/plugins/com/velti/SendToFriendViaEmail/backEnd/SendEmailService',
        facebook_url:'http://m.corninggorillaglass.com/fb/share/share.jsp?p=',
        faq_service:'http://gorilla.v4en.com/rssReader/gorilla/faqs',
        media_service:'http://gorilla.v4en.com/rssReader/gorilla/media',
        consumers_service:'http://gorilla.v4en.com/rssReader/gorilla/consumers',
        submit_service:'http://gorilla.v4en.com/rssReader/gorilla/comment',
        sms_service:'http://10.64.102.201:8888/message-toolkit/messagerequests',
        google_analytics_id:'UA-33212028-1',
        visualize_analytics_url:'visualize-greece.velti.com',
    	visualize_analytics_siteId:'22',
    	visualize_analytics_EventssiteId:'22'
    }];
    
    if (document.location.href.search('gorilla-stag.v4en') > -1)
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    else if ((document.location.href.search('corninggorillaglass') > -1) || (document.location.href.search('gorilla.v4en') > -1) )
    {
        //live url
    	gorilla.services=objServices[1];
    }
    else if (document.location.href.search('localhost') > -1)
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    else
    {
        //staging url
    	gorilla.services=objServices[0];
    }
    
})();


//---------------------Popup Blocker----------------
gorilla.closePopupBlocker = function()
{
	var popup = document.getElementById("popupBlocker");
	popup.style.display = "none";
}


gorilla.showPopupBlocker =function()
{
	
	var popup = document.getElementById("popupBlocker")
	popup.style.display = "block";
}

gorilla.openExternalLink = function(mylink)
{
	
	var newwin = window.open(mylink,'_blank');
    if(!newwin){
		gorilla.showPopupBlocker();
    }
}
//---------------------Popup Blocker----------------

gorilla.showPageById = function(id)
{	
	//alert(id);
	veltijs.showPage = {'type':"noswipe"};
	location.hash=id;	
};


gorilla.createPageControllers = function()
{
	veltijs.analytics();
	veltijs.setAnalyticsEvents(document);
	
	var pageControllers = document.getElementsByClassName('p_controller');
	for(var i=0;i<pageControllers.length;i++)
	{
		var dotsEnabled = false;
		var timer = false;
		var loop = false;
		var swipeEnabled = true;
	 	if(pageControllers[i].getAttribute("rel")=="carouzel"){
			//if this  controller is a carouzel
	 		var dotsEnabled = true;
			var timer = true;
			var loop = true;
			if(pageControllers[i].children[0].id=="products_page_carouzel")
			{
				var dotsEnabled = false;
				var timer = true;
				var loop = true;
				var swipeEnabled = false;
			}
		}else{
			//if this  controller is page
			
			pages_Controller
			if(pageControllers[i].children[0].id=="pages_Controller")
			{
				var loop = true;
			}
			
			if(pageControllers[i].children[0].id=="main_Controller")
			{
				var swipeEnabled = false;
				
			}
			if(pageControllers[i].children[0].id=="ext_pages_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="innovation_Controller")
			{
				var swipeEnabled = false;
			}
			if(pageControllers[i].children[0].id=="video_Controller")
			{
				var swipeEnabled = false;
			}	
		}
		veltijs.newPageControllerParser(pageControllers[i].children[0].id,loop,0,swipeEnabled,dotsEnabled,timer);
	}
	window.addEventListener("hashchange", veltijs.onlocationchange, false);
	gorilla.coverflow(document.getElementById('coverflow'));
	
	gorilla.createVideoCarousels();
};


gorilla.displaySwipeBtns = function(disp)
{
	var swipe_btn_left = document.getElementById("swipe_btn_left");
	var swipe_btn_right = document.getElementById("swipe_btn_right");
	if(disp){
		swipe_btn_left.style.display="block";
		swipe_btn_right.style.display="block";
	}else{
		swipe_btn_left.style.display="none";
		swipe_btn_right.style.display="none";
	}
};

gorilla.productPages = [
		         "Acer_page","Asus_page","Dell_page",
		         "Htc_page","Lg_page","Motorola_page",
		         "Nec_page","Nokia_page","Samsung_page",
		         "SK_Telesys_page","Sonim_page","Lenovo_page",
		         "Motion_Computing_page","Hyundai_page","Sony_page",
		         "HP_page","AboutUs_page","ContactUs_page",
		         "ProductsFull_page","SamsungSUR40_page","Lumigon_page"];

gorilla.extPages=['SpecificNewsAndFeeds_page',
                  'AllNewsAndFeeds_page',
                  'GorillaEmail_page',
                  'GorillaSMS_page',
                  'AllPraise_page',
                  'PraiseThankYou_page',
                  'WallpaperPreview_page',
                  'VideoPreview_page'];


gorilla.isExternal = false;

gorilla.setVideoOrientation = function(o,page)
{
	var video_Controller =  document.getElementById('video_Controller');
	
	var videoPages = video_Controller.children;
	if(page=="")
		{
			page= videoPages[video_Controller.activePage];
		}
	
	if(o=='l')
		{
			for(var v=0;v<videoPages.length;v++)
			{
				videoPages[v].children[1].style.display="block";
				videoPages[v].children[0].style.display="none";
			}
			if(page.id!="videos_page")
			selectedVideoToShare = Math.abs(page.children[1].children[1].position);
		}
	else{
		for(var v=0;v<videoPages.length;v++)
		{
			videoPages[v].children[1].style.display="none";
			videoPages[v].children[0].style.display="block";
		}
		if(page.id!="videos_page")
		{
			if(page.children[0].children[0].id.search("carouselThumb") >-1){
				selectedVideoToShare = Math.abs(page.children[1].children[1].position);
			}
			else{
				selectedVideoToShare = Math.abs(page.children[0].children[0].position);
			}
		}
	}
	
}

gorilla.pageTransitionStartListeners = function(leave_page,enter_page){
	
	var o = veltijs.getOrientation();
	if(enter_page.parentNode.isCarousel)
	{	
		// This is a carousel
		
	}
	else
	{		
		document.getElementById("previewVideoCont").pause();
		
		var menu = document.getElementById("main_menu_cont");
		menu.style.display = "block";
		gorilla.isExternal = false;
		if((enter_page.id!='internal_pages')&&(enter_page.id!='external_pages')){
			if((enter_page.id=='innovating_page')||(enter_page.id=='videos_page'))
			{	
				var hashVal = location.hash;
				var page = hashVal.replace('#','');
				if(page=='innovating_page'||page=='videos_page')
				{					
					veltijs.analyticsTrackPage(enter_page.getElementsByClassName('page active')[0]);					
				}else{
					veltijs.analyticsTrackPage(document.getElementById(page));
				}
			}
			else if((enter_page.id=='overview_page')||
					(enter_page.id=='characteristics_page')||
					(enter_page.id=='customization_page')||
					(enter_page.id=='applications_page')|| 
					(enter_page.id=='literature_page')||
					(enter_page.id=='victor_page')||
					(enter_page.id=='demos_page')||
					(enter_page.id=='saying_page')||
					(enter_page.id=='brought_page')
					)
			{	
				//veltijs.analyticsTrackPage(enter_page.getElementsByClassName('page active')[0]);
			}
			else{
			veltijs.analyticsTrackPage(enter_page);
			}
		}
		
		gorilla.displaySwipeBtns(true);
		
		if(enter_page.parentNode.id == 'pages_Controller')
		{
			//Change the menu btns style
			gorilla.changeMainMenuBtnsStyle(enter_page.id.replace('page','menu_btn'));
		}
		
		
		if(gorilla.productPages.indexOf(enter_page.id)>-1)
		{			
			gorilla.isExternal = enter_page;
			
			var ext_page = 'product_pages/'+enter_page.id+'.html';
			//gorilla.switchLoadingIndicator(true);
			veltijs.sendRequest(ext_page, 'GET',true, null, gorilla.externalPageParse);
			gorilla.changeMainMenuBtnsStyle('products_menu_btn');
			gorilla.displaySwipeBtns(false);
		}
		
		if(gorilla.extPages.indexOf(enter_page.id)>-1)
		{			
			
			gorilla.displaySwipeBtns(false);
			if(enter_page.id == 'WallpaperPreview_page')
			{				
				menu.style.display = "none";
			}
			
		}
		
		
		switch(enter_page.id)
		{
			case 'home_page':
				break;
			case 'products_page':
				break;
			case 'news_page':
				if(!gorilla.newsEventsCreated)
				gorilla.getNewsAndEventsPage();
				break;
			case 'innovating_page':
				var inno_menu = document.getElementById('inno_menu');
				for(var i=0;i<inno_menu.children.length;i++)
				{
					inno_menu.children[i].style.color="#AEAEAE";
				}
				var innovation_Controller =  document.getElementById('innovation_Controller');
				inno_menu.children[innovation_Controller.activePage].style.color="#009ED3";				
				break;		
			case 'overview_page':
				gorilla.setInnovatingpageMenu(0);
				break;
			case 'characteristics_page':
				gorilla.setInnovatingpageMenu(1);
				break;
			case 'customization_page':
				gorilla.setInnovatingpageMenu(2);
				break;
			case 'applications_page':
				gorilla.setInnovatingpageMenu(3);
				break;
			case 'literature_page':
				gorilla.setInnovatingpageMenu(4);
				break;			
			case 'AllNewsAndFeeds_page':
				scrollTo(0,0);
				break;				
			case 'SpecificNewsAndFeeds_page':
				scrollTo(0,0);
				break;					
			case 'faqs_page':
				if(!gorilla.faqCreated)
				gorilla.getFaqPage();
				break;
			case 'GorillaEmail_page':
				gorilla.clearEmaillabel();
				break;					
			case 'GorillaSMS_page':				
				gorilla.clearSmslabel();
				break;				
			case 'praise_page':
				if(!gorilla.praiseCreated)			
				gorilla.getPraisePage();
				break;					
			case 'videos_page':
				var video_menu = document.getElementById('video_menu');
				for(var i=0;i<video_menu.children.length;i++)
				{
					video_menu.children[i].style.color="#AEAEAE";
				}
				var video_Controller =  document.getElementById('video_Controller');
				video_menu.children[video_Controller.activePage].style.color="#009ED3";	
				gorilla.setVideoOrientation(o,enter_page);
				//var vidCont = document.getElementById("previewVideoCont");
				//vidCont.stop();				
				break;			
			case 'victor_page':					
				gorilla.setVideopageMenu(0);				
				gorilla.setVideoOrientation(o,enter_page)
				break;
			case 'demos_page':
				gorilla.setVideopageMenu(1);				
				gorilla.setVideoOrientation(o,enter_page)
				break;
			case 'saying_page':
				gorilla.setVideopageMenu(2);			
				gorilla.setVideoOrientation(o,enter_page)
				break;
			case 'brought_page':
				gorilla.setVideopageMenu(3);				
				gorilla.setVideoOrientation(o,enter_page)
				break;
			case 'ContactUs_page':
				gorilla.changeMainMenuBtnsStyle();
				break;
			case 'AboutUs_page':
				gorilla.changeMainMenuBtnsStyle();
				break;
			default:
				break;
		}
		switch(leave_page.id)
		{			
			case 'home_page':
				break;
			case 'products_page':				
				break;
			
			case 'videos_page':
			
				break;			
			case 'victor_page':
			
				break;
			case 'demos_page':
				
				break;
			case 'saying_page':
				
				break;
			case 'brought_page':
				break;
			default:
				break;
		}	
	}
};


gorilla.pageTransitionEndListeners = function(leave_page,enter_page)
{
	if(enter_page.parentNode.isCarousel)
	{
		// this is a carousel not a page 
		
	}else
	{
		//This is a page
		
		
		switch(enter_page.id)
		{
			case 'home_page':
				break;
			case 'products_page':
				break;
			case 'innovating_page':
				break;
			default:
				break;
		}
		
		switch(leave_page.id)
		{
			case 'home_page':
				break;
			case 'products_page':
				break;
			default:
				break;
		}
	}	
};



//----------------|Menu|------------------//

gorilla.gotToPage = function(pageToGo)
{
	var pagename = pageToGo.id.replace('menu_btn','page');		
	
	gorilla.showPageById(pagename);
};



gorilla.changeMainMenuBtns = function(pageToGo)
{
	
	gorilla.changeMainMenuBtnsStyle(pageToGo.id);
	gorilla.gotToPage(pageToGo);
};

gorilla.changeMainMenuBtnsStyle = function(menuname)
{
		
		var menu1_cont = document.getElementById("menu1_cont");
		for(var i=0;i< menu1_cont.children.length;i++)
		{
			menu1_cont.children[i].style.backgroundPosition="0 0";
		}
		var menu2_cont = document.getElementById("menu2_cont");
		for(var i=0;i< menu2_cont.children.length;i++)
		{
			menu2_cont.children[i].style.backgroundPosition="0 0";
		}
		
		if(menuname!=undefined){
			var item = document.getElementById(menuname);
			item.style.backgroundPosition="0 -18";	
		}
};
//----------------|Menu End|------------------//



//------------------|Accessing External Pages|--------------------//
gorilla.openExternalPage = function(el)
{
	var pagename = el.id.replace('btn','page');		
	gorilla.switchLoadingIndicator(true);
	gorilla.showPageById(pagename);
};

gorilla.switchLoadingIndicator = function(sw)
{
	scrollTo(0,0);
	var el = document.getElementById('loadingIndicator');
	el.ontouchmove=function(e)
	{
		e.cancelBubble = true;
		e.preventDefault();
	}
	
	if(sw)
		el.style.display="block";
	else
	   el.style.display="none";
};


gorilla.externalPageRunScripts = function(el)
{
	var scripts  = el.getElementsByTagName('script');

	if(scripts.length>0){
		for(var i=0; i<scripts.length; i++)
		{
			eval(scripts[i].innerHTML);
		}
	}
}

gorilla.externalPageParse = function(req)
{
	
	var el = document.getElementById(location.hash.replace("#",''));
	el.innerHTML = req.responseText;
	gorilla.switchLoadingIndicator(false);	
	veltijs.setAnalyticsEvents(el);

	if(veltijs.imagesPreloaded){
		console.log("running ext scripts from within")
		gorilla.externalPageRunScripts(el);
	}
	
};

//------------------|Accessing External Pages END|--------------------//




//---------------------|News and Events Page|-------------------------//
gorilla.newsEventsCreated = false;

gorilla.getNewsAndEventsPage = function()
{
	veltijs.proxy.sendRequest(gorilla.services.press_release_service, 'GET',true, null, gorilla.newsReleasesParse);
	veltijs.proxy.sendRequest(gorilla.services.news_service, 'GET', true, null, gorilla.inThenewsParse);
	veltijs.proxy.sendRequest(gorilla.services.events_service, 'GET', true, null, gorilla.EventsParse);
	
	gorilla.newsEventsCreated = true; //This is to run once.	
};


gorilla.newsReleasesParse = function(req)
{
	var el = document.getElementById("news_releases");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<4; i++)
	{		
		var retObj = {
			mytitle : rssjson[i].title,
	        mydescription : rssjson[i].description,
	        mysubtitle : rssjson[i].subtitle,
		    mylocation : rssjson[i].location,
		    mydate : rssjson[i].formattedDateItem+" -"
		};
	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='11px';
	    	div_date.style.height='12px';
	    	div_date.style.paddingTop='6px';
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';        	
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.data = retObj;        	
        	div_title.onclick = function()
        	{        		        		
        		document.getElementById("specific_title_label").innerHTML='NEWS RELEASES';
        		document.getElementById("specific_name_label").innerHTML=this.data.mytitle.toUpperCase();  
        		document.getElementById("specific_titleUpper_label").innerHTML=this.data.mytitle.toUpperCase();          		        		
        		
        		document.getElementById("specific_subtitle_label").style.display='block';
        		document.getElementById("specific_subtitle_label").innerHTML=''; 
        		if (this.data.mysubtitle != null)
        		{
        			document.getElementById("specific_subtitle_label").style.display='block';
        			document.getElementById("specific_subtitle_label").innerHTML=this.data.mysubtitle.toUpperCase();  
        		}
        		        		
        		var paragraph = this.data.mydescription.substring(0,3);          		
                var text = this.data.mydescription.substring(3);               
                                                                
                if (this.data.mylocation == null)
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mydate+'</span>' + " " + text;
                }
                else
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mylocation+", "+this.data.mydate+'</span>' + " " + text;
                }                                                                                                
        		
                
        		var el = document.getElementById("specificNewsAndEvents");
        		el.innerHTML = whole_text;        		
        		gorilla.showPageById("SpecificNewsAndFeeds_page");
        	};
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
        
       
        el.appendChild(div_date);
        el.appendChild(div_title);	
	}
};


gorilla.inThenewsParse = function(req)
{
	var el = document.getElementById("in_the_news");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<3; i++)
	{		
    
	    var retObj = {
    		mytitle : rssjson[i].title,
    		mylink : rssjson[i].link	        
		};
	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='11px';	  
	    	div_date.style.height='12px';
	    	div_date.style.paddingTop='6px';
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.data = retObj; 
        	div_title.onclick= function()
        	{        		        		
        		gorilla.openExternalLink(this.data.mylink);
        	};
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
               
        el.appendChild(div_date);
        el.appendChild(div_title);	
	}
};


gorilla.EventsParse = function(req)
{
	var el = document.getElementById("events_cont");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	var total_num = rssjson.length;
	
	if (total_num != 0)
	{		
	
		//show container if there are events
		document.getElementById("events_out_container").style.display="block";
		
		for (var i=0; i<total_num; i++)
		{			    
		    var retObj = {
	    		mytitle : rssjson[i].title,
	    		mydescription : rssjson[i].description	        
			};	    
		    	
	        var div_title = document.createElement('div');
	        	div_title.style.color='#009ED3';
	        	div_title.style.fontSize='11px';
	        	div_title.style.fontWeight = 'bold';
	        	div_title.data = retObj; 
	        	div_title.style.paddingTop='6px';
	        	div_title.onclick= function()
	        	{        		        		
	        		document.getElementById("specific_title_label").innerHTML = 'EVENTS';
	        		document.getElementById("specific_name_label").innerHTML = this.data.mytitle.toUpperCase();  
	        		
	        		document.getElementById("specific_titleUpper_label").innerHTML = this.data.mytitle.toUpperCase();          		        		        		
	        		document.getElementById("specificNewsAndEvents").innerHTML = this.data.mydescription;   		
	        		
	        		//show page
	        		gorilla.showPageById("SpecificNewsAndFeeds_page");
	        	};
	        	
	        var div_description = document.createElement('div');
	        	div_description.style.color='#5f5d5f';
	        	div_description.style.fontSize='11px';
	        	div_description.data = retObj;         	
	        	        
	        div_title.innerHTML = retObj.mytitle;
	        div_description.innerHTML = retObj.mydescription;
	                
	        el.appendChild(div_title);
	        el.appendChild(div_description);			
		}//end for
		
	}//end if
		
	
};


//---------------------|News and Events Page End|-------------------------//




//---------------------|AllNewsAndEvents Page|-------------------------//
gorilla.getAllNewsAndEventsPage = function(btn)
{		
	gorilla.switchLoadingIndicator(true);
	if(btn.id=="allreleases_btn")
	{		
		document.getElementById("title_label").innerHTML = "NEWS RELEASES";			
		veltijs.proxy.sendRequest(gorilla.services.showall_press_release_service, 'GET', true, null, gorilla.AllNewsReleasesParse);	
	}
	else if(btn.id=="allnews_btn")
	{
		document.getElementById("title_label").innerHTML = "IN THE NEWS";	
		veltijs.proxy.sendRequest(gorilla.services.showall_news_service, 'GET', true, null, gorilla.AllNewsItemsParse);	
	}	
	
};


gorilla.AllNewsReleasesParse = function(req)
{
	var el = document.getElementById("allNewsAndEvents");
	el.innerHTML="";
	
	var rssjson = JSON.parse(req.responseText);
	for (var i=0; i<rssjson.length; i++)
	{		
	    
	    var retObj = {
			mytitle : rssjson[i].title,
	        mydescription : rssjson[i].description,
	        mysubtitle : rssjson[i].subtitle,
		    mylocation : rssjson[i].location,
		    mydate : rssjson[i].formattedDateItem+" -"
		};
	    	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='11px';	
	    	div_date.style.height='12px';
	    	div_date.style.paddingTop='6px';
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.data = retObj;
        	div_title.onclick= function()
        	{        		        		
        		document.getElementById("specific_title_label").innerHTML='NEWS RELEASES';
        		document.getElementById("specific_name_label").innerHTML=this.data.mytitle.toUpperCase();  
        		document.getElementById("specific_titleUpper_label").innerHTML=this.data.mytitle.toUpperCase();          		        		
        		
        		document.getElementById("specific_subtitle_label").style.display='block';
        		document.getElementById("specific_subtitle_label").innerHTML=''; 
        		if (this.data.mysubtitle != null)
        		{
        			document.getElementById("specific_subtitle_label").style.display='block';
        			document.getElementById("specific_subtitle_label").innerHTML=this.data.mysubtitle.toUpperCase();  
        		}
        		        		
        		var paragraph = this.data.mydescription.substring(0,3);          		
                var text = this.data.mydescription.substring(3);
                
                
                if (this.data.mylocation == null)
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mydate+'</span>' + " " + text;
                }
                else
                {
                	var whole_text = paragraph + '<span style="color:#009ed3;font-weight:bold;font-size:11px">'+this.data.mylocation+", "+this.data.mydate+'</span>' + " " + text;
                }
                    		
        		
        		var el = document.getElementById("specificNewsAndEvents");
        		el.innerHTML = whole_text;        		
        		gorilla.showPageById('SpecificNewsAndFeeds_page');
        	};
        	
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
        
        el.appendChild(div_date);
        el.appendChild(div_title);	
	}	
	gorilla.switchLoadingIndicator(false);	
	gorilla.showPageById('AllNewsAndFeeds_page');
};


gorilla.AllNewsItemsParse = function(req)
{
	var el = document.getElementById("allNewsAndEvents");
	el.innerHTML="";
	
	var rssjson = JSON.parse(req.responseText);
	for (var i=0; i<rssjson.length; i++)
	{		                           
	    var retObj = {
    		mytitle : rssjson[i].title,
    		mylink : rssjson[i].link	        
		};
	    
	    var div_date = document.createElement('div');
	    	div_date.style.color='#5f5d5f';
	    	div_date.style.fontSize='11px';	 
	    	div_date.style.height='12px';
	    	div_date.style.paddingTop='6px';
	    	
        var div_title = document.createElement('div');
        	div_title.style.color='#009ED3';
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.data = retObj;
        	div_title.onclick= function()
        	{        		        		
        		gorilla.openExternalLink(this.data.mylink);
        	};
        	
        
        div_date.innerHTML = rssjson[i].formattedDate;
        div_title.innerHTML = retObj.mytitle;
        
        el.appendChild(div_date);
        el.appendChild(div_title);	
	}
	
	gorilla.switchLoadingIndicator(false);	
	gorilla.showPageById('AllNewsAndFeeds_page');
};
//---------------------|AllNewsAndEvents Page End|-------------------------//



//---------------------|FAQ Page|-------------------------//
gorilla.faqCreated = false;

gorilla.getFaqPage = function()
{
	veltijs.proxy.sendRequest(gorilla.services.faq_service, 'GET', true, null, gorilla.faqPageParse);
		
	gorilla.faqCreated = true; //This is to run once.	
};

gorilla.faqPageParse = function(req)
{
	var el = document.getElementById("faq_cont");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);

	for (var i=0; i<rssjson.length; i++)
	{		
    
	    var div_question = document.createElement('div');
	    	div_question.style.color='#000';
	    	div_question.style.fontSize='11px';	    	
	    	div_question.style.paddingTop='6px';
	    	div_question.style.fontWeight = 'bold';
	    	
        var div_desc = document.createElement('div');
	        div_desc.style.color='#000';        	
	        div_desc.style.fontSize='11px';        		             
	                
	        
	        div_question.innerHTML = rssjson[i].title;
	        if ((rssjson[i].description.charAt(0)=="<")&&(rssjson[i].description.charAt(1))=="p")
		    {
	        	div_desc.innerHTML = rssjson[i].description;
		    }
		    else
		    {
		    	div_desc.innerHTML = "<p>"+rssjson[i].description+"</p>";
		    }
        
       
        el.appendChild(div_question);
        el.appendChild(div_desc);	
	}
};
//---------------------|FAQ Page End|-------------------------//



//---------------------|Praise Page|-------------------------//
gorilla.limitText = function (limitField, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} 
}

gorilla.praiseCreated = false;

gorilla.getPraisePage = function()
{
	//every time enter page clear fields
	var nameField = document.getElementById('praise_name');
	var commentsField = document.getElementById('praise_comments');
	var validationPraiseField = document.getElementById('validation_praise');	
	nameField.value = "";
	commentsField.value = "";
	validationPraiseField.innerHTML = "";
			
	veltijs.proxy.sendRequest(gorilla.services.media_service, 'GET', true, null, gorilla.mediaParse);
	veltijs.proxy.sendRequest(gorilla.services.consumers_service, 'GET', true, null, gorilla.consumersParse);
	
	gorilla.praiseCreated = true; //This is to run once.	
};


gorilla.mediaParse = function(req)
{
	var el = document.getElementById("media_releases");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<2; i++)
	{		
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
			mylegal : rssjson[i].legal,
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),	       
	        mydate : rssjson[i].formattedDate,
	        mylink : rssjson[i].link,		   		   
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='11px';
    		if (i==0)
    		{
    			div_desc.style.paddingTop='5px';
    		}
    		else
    		{
    			div_desc.style.paddingTop='20px';
    		}
	    
    	var flag_media = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_media = "true";	
    	}
    	
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='11px';
    	    
        var div_link = document.createElement('div');
        	div_link.style.color='#009ED3';
        	div_link.style.fontSize='11px';    
        	div_link.style.width='80px';
        	div_link.style.fontWeight = 'bold';
        	div_link.data = retObj;
        	div_link.onclick= function()
        	{        		        		
        		gorilla.openExternalLink(this.data.mylink);
        	};
        	
        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');       
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
        div_date.innerHTML = retObj.mydate;
        div_link.innerHTML = "Learn More >>";
        
        el.appendChild(div_desc);
        if (flag_media == "true"){el.appendChild(div_legal);}        
        el.appendChild(div_title);	
        el.appendChild(div_date);	
        el.appendChild(div_link);	
        
	}
};

gorilla.consumersParse = function(req)
{
	var el = document.getElementById("consumers_releases");
	el.innerHTML = "";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<3; i++)
	{		
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),       
	        mydate : rssjson[i].formattedDate,
	        mylegal : rssjson[i].legal,
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='11px';    		
    		div_desc.style.paddingTop='2px';
    		
		var flag_cons = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_cons = "true";	
    	}
    		    		    		    		    		
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='11px';
    	    div_date.style.paddingBottom='10px';	

        	        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
        div_date.innerHTML = retObj.mydate;      
        
        el.appendChild(div_desc);
        if (flag_cons == "true"){el.appendChild(div_legal);}   
        el.appendChild(div_title);	
        el.appendChild(div_date);	              
	}
};
//---------------------|Praise Page End|-------------------------//



//---------------------|AllPraise Page|-------------------------//
gorilla.getAllPraisePage = function(btn)
{		
	gorilla.switchLoadingIndicator(true);
			
	if (btn.id=="allmedia_btn")
	{				
		//hide submit form in case of media
		document.getElementById("allpraise_cont_new").style.display="none";
	
		document.getElementById("praise_label").innerHTML = "MEDIA";	
		document.getElementById("praise_label1").innerHTML = "MEDIA";	
		veltijs.proxy.sendRequest(gorilla.services.media_service, 'GET', true, null, gorilla.AllMediaParse);	
	}
	else if (btn.id=="allconsumers_btn")
	{
		//show submit form in case of consumers
		document.getElementById("allpraise_cont_new").style.display="block";
		
		document.getElementById("praise_label").innerHTML = "CONSUMERS";
		document.getElementById("praise_label1").innerHTML = "CONSUMERS";	
		veltijs.proxy.sendRequest(gorilla.services.consumers_service, 'GET', true, null, gorilla.AllConsumersParse);
	}		
};


gorilla.AllMediaParse = function(req)
{
	var el = document.getElementById("allpraise_cont");
	el.innerHTML="";
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<10; i++)
	{		
	    
		var retObj = {
			mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
	        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),	       
	        mydate : rssjson[i].formattedDate,
	        mylink : rssjson[i].link,	
	        mylegal : rssjson[i].legal,
		};				
		
	    var div_desc = document.createElement('div');
    		div_desc.style.color='#000';
    		div_desc.style.fontSize='11px';
    		if (i==0)
    		{
    			div_desc.style.paddingTop='5px';
    		}
    		else
    		{
    			div_desc.style.paddingTop='20px';
    		}
    		
    	var flag_all = "false";	
    	if (retObj.mylegal != null)
    	{
	    	var div_legal = document.createElement('div');
		    	div_legal.style.color='#5f5d5f';        	
		    	div_legal.style.fontSize='9px';
		    	div_legal.style.paddingTop='10px';		    	
		    	div_legal.innerHTML = retObj.mylegal;	
		    	
		    	flag_all = "true";	
    	}	
    		    		    		
        var div_title = document.createElement('div');
        	div_title.style.color='#5f5d5f';        	
        	div_title.style.fontSize='11px';
        	div_title.style.fontWeight = 'bold';
        	div_title.style.paddingTop='10px';
        	
    	var div_date = document.createElement('div');
    	    div_date.style.color='#5f5d5f';
    	    div_date.style.fontSize='11px';
    	    
        var div_link = document.createElement('div');
        	div_link.style.color='#009ED3';
        	div_link.style.fontSize='11px';    
        	div_link.style.width='80px';
        	div_link.style.fontWeight = 'bold';
        	div_link.data = retObj;
        	div_link.onclick= function()
        	{        		        		
        		gorilla.openExternalLink(this.data.mylink);
        	};
        	
        	        	       	        				          
        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');
        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
        div_date.innerHTML = retObj.mydate;
        div_link.innerHTML = "Learn More >>";
        
        el.appendChild(div_desc);
        if (flag_all == "true"){el.appendChild(div_legal);}   
        el.appendChild(div_title);	
        el.appendChild(div_date);	
        el.appendChild(div_link);	
	}	
	
	gorilla.switchLoadingIndicator(false);
	gorilla.showPageById('AllPraise_page');
};


gorilla.AllConsumersParse = function(req)
{
	var el = document.getElementById("allpraise_cont");
	el.innerHTML="";	
	var rssjson = JSON.parse(req.responseText);
	
	for (var i=0; i<25; i++)
	{		                           
		var retObj = {
				mytitle : rssjson[i].title.replace(/&quot;/g,'"'),
		        mydescription : rssjson[i].description.replace(/&quot;/g,'"'),       
		        mydate : rssjson[i].formattedDate,	        	   		   
			};				
			
		    var div_desc = document.createElement('div');
	    		div_desc.style.color='#000';
	    		div_desc.style.fontSize='11px'; 
	    		if (i==0)
	    		{
	    			div_desc.style.paddingTop='0px';
	    		}
	    		else
	    		{
	    			div_desc.style.paddingTop='12px';
	    		}
	    		
	    		    		    		    		    		
	        var div_title = document.createElement('div');
	        	div_title.style.color='#5f5d5f';        	
	        	div_title.style.fontSize='11px';
	        	div_title.style.fontWeight = 'bold';
	        	div_title.style.paddingTop='2px';
	        	
	    	var div_date = document.createElement('div');
	    	    div_date.style.color='#5f5d5f';
	    	    div_date.style.fontSize='11px';

	        	        	        	       	        				          
	        div_desc.innerHTML = retObj.mydescription.replace(/&reg;/g,'®');
	        div_title.innerHTML = retObj.mytitle.replace(/&reg;/g,'®');
	        div_date.innerHTML = retObj.mydate;      
	        
	        el.appendChild(div_desc);
	        el.appendChild(div_title);	
	        el.appendChild(div_date);	 
	}	
	
	gorilla.switchLoadingIndicator(false);
	gorilla.showPageById('AllPraise_page');
	
};
//---------------------|AllNewsAndEvents Page End|-------------------------//





//---------------------|Innovating Page Start|-------------------------//

gorilla.setPage = function(pageToGo)
{
	var pagename = pageToGo.id.replace('btn','page');		
	gorilla.showPageById(pagename);
};

gorilla.setInnovatingpageMenu=function(num)
{
	var inno_menu = document.getElementById('inno_menu');
	for(var i=0;i<inno_menu.children.length;i++)
	{
		inno_menu.children[i].style.color="#AEAEAE";
	}
	inno_menu.children[num].style.color="#009ED3";
	var inno_menu_dots = document.getElementById('inno_menu_dots');
	for(var i=0;i<inno_menu_dots.children.length;i++)
	{
		inno_menu_dots.children[i].className="";
	}
	inno_menu_dots.children[num].className="active_inno_menu";
};


var label_id = '';
gorilla.openPDF = function(el)
{
    label_id = el.id; 
    //alert(label_id);
    var url ='';
    
    if (label_id == 'thin_pdf')
    {    	
    	veltijs.pushCustomEventAnalytics('thin_pdf');
    	url = "assets/PDFs/Thin Glass_ A New Design Reality_PDF.pdf";  
    }
    else if (label_id == 'product_pdf')
    {
    	veltijs.pushCustomEventAnalytics('product_pdf');
    	url = "assets/PDFs/Corning Gorilla Glass 2 Product Information Sheet.pdf"; 
    }
    else if (label_id == 'protective_pdf')
    {
    	veltijs.pushCustomEventAnalytics('protective_pdf');
    	url = "assets/PDFs/Protective Cover Glass for Portable Display Devices _PDF.pdf";    
    }
    else if (label_id == 'controlled_pdf')
    {
    	veltijs.pushCustomEventAnalytics('controlled_pdf');
    	url = "assets/PDFs/Controlled Edge Damage by Dynamic Impact_PDF.pdf";    
    }
    else if (label_id == 'easy_pdf')
    {
    	veltijs.pushCustomEventAnalytics('easy_pdf');
    	url = "assets/PDFs/Easy-to-Clean Surfaces for Mobile Devices_PDF.pdf";    
    }
    else if (label_id == 'specialty_pdf')
    {
    	veltijs.pushCustomEventAnalytics('specialty_pdf');
    	url = "assets/PDFs/Specialty GlassA New Design Element in Consumer Electronics _PDF.pdf";    
    }
    else if (label_id == 'cover_pdf')
    {
    	veltijs.pushCustomEventAnalytics('cover_pdf');
    	url = "assets/PDFs/Corning Gorilla Glass for Large Cover Applications.pdf";    
    }
    
    setTimeout(function(){
    	gorilla.openExternalLink(url);
	},100)
		
};
//---------------------|Innovating Page End|-------------------------//

// -----------------------|VIDEO PAGE|-----------------------------//


gorilla.setVideopageMenu=function(num)
{
	var inno_menu = document.getElementById('video_menu');
	for(var i=0;i<inno_menu.children.length;i++)
	{
		inno_menu.children[i].style.color="#AEAEAE";
	}
	inno_menu.children[num].style.color="#009ED3";
	var inno_menu_dots = document.getElementById('video_menu_dots');
	for(var i=0;i<inno_menu_dots.children.length;i++)
	{
		inno_menu_dots.children[i].className="";
	}
	inno_menu_dots.children[num].className="active_inno_menu";
};


gorilla.setVideoPreviewPage = function(url)
{
	//alert(2);
	var vidCont = document.getElementById("previewVideoCont");
	vidCont.autoplay=false;
	vidCont.src = url;
	vidCont.load();
	if(navigator.userAgent.toLowerCase().search('froyo') > -1)
	{
		vidCont.addEventListener("touchend",function(){this.play();});
	}
	
	gorilla.showPageById("VideoPreview_page");
}


gorilla.createVideoCarousels = function()
{
	 var videoToCreate ={
			victor:
			{
				divName:"carouselVideo1",
	            labelName:"carouselThumb1",
	            container:"videocont1",
	            containerl:"videocont1l",
	            pics:['assets/videosPage/channel_surfing.jpg',
	                  'assets/videosPage/cooking_up.jpg',
	                  'assets/videosPage/king_of_the_office.jpg'],
	            labels:['assets/videosPage/channel_surfing_label.png',
	                    'assets/videosPage/cooking_up_label.png',
	                    'assets/videosPage/king_of_the_office_label.png'],          
	            actions:[
	                    function()
	                    {      	        	                    	
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor1');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/TV_30_bdcst_mix_.mp4");
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Channel Surfing", "Videos - Main"];	                    	
	                    },
	                    function()                    
	                    { 	        
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor2');
	                    		                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Kitchen30_bdcast_mix_1080p_.mp4");
	                    		                    	
	                    	// var args = ["trackEvent", "GorillaVideos", "Video Views", "Cooking Up Tomorrow's Kitchen", "Videos - Main"];
	                    },
	                    function()
	                    {	                    	 
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('victor3');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Office_30_bdcst_mix_.mp4");
	                    		                    		                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "King of the Office", "Videos - Main"];
	                    }
	                ]
			},
			demos:
			{
				divName:"carouselVideo2",
				labelName:"carouselThumb2",
				container:"videocont2",
				containerl:"videocont2l",
				pics:['assets/videosPage/how_corning_tests.jpg'],
				labels:['assets/videosPage/how_corning_tests_label.png'],
				actions :[
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('demos1');
	                	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/How_Corning_tests_Glass.mp4");
	                		                	                    	
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "How Corning Tests Gorilla Glass", "Videos - Main"];
	                }
	            ]
			},
			saying:
	        {
				divName:"carouselVideo3",
				labelName:"carouselThumb3",
	            container:"videocont3",
	            containerl:"videocont3l",
	            pics:['assets/videosPage/gorilla_tough.jpg',
	                  'assets/videosPage/microsoft.jpg'],
	            labels:['assets/videosPage/gorilla_tough_label.png',
	                    'assets/videosPage/microsoft_label.png'],
	            actions:[
	                    function()
	                    {
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('saying1');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Gorilla_Tough.mp4");	                    		                    
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Gorilla Tough", "Videos - Main"];	    	                
	                    },
			            function()
			            {
	                    	//analytics
	                    	veltijs.pushCustomEventAnalytics('saying2');
	                    	
	                    	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Gorilla_2_and_Microsoft.mp4");	                    		                    	
	                    	
	                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos - Main"];
			            }
	            ]
	        },
	        brought:
	        {
	          divName:"carouselVideo4",
	          labelName:"carouselThumb4",
	          container:"videocont4",
	          containerl:"videocont4l",
	          pics:['assets/videosPage/a_day_made_of.jpg',
	                'assets/videosPage/design.jpg',
	                'assets/videosPage/the_way_we_think.jpg'],
	          labels:['assets/videosPage/a_day_made_of_label.png',
	                  'assets/videosPage/design_label.png',
	                  'assets/videosPage/the_way_we_think_label.png'],
	          actions:[
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought1');
                    	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Corning_ADayMadeofGlass.mp4");    	                	                	
                    	
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "A Day Made of Glass", "Videos - Main"];			            
	                },
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought2');
                    	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Corning_modern_design_video.mp4");
    	                
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Corning Gorilla Glass for Seamless Elegant Design", "Videos - Main"];			            
	                },
	                function()
	                {
	                	//analytics
                    	veltijs.pushCustomEventAnalytics('brought3');
                    	
	                	gorilla.setVideoPreviewPage("http://download.velti.gr.edgesuite.net/gor/Changing the Way We Think about Glass.mp4");    	             	                
                    
                    	//var args = ["trackEvent", "GorillaVideos", "Video Views", "Changing the Way We Think about Glass", "Videos - Main"];			            
	                }
	            ]
	        }
		}
		
	 	
	 	for(var i=0;i<Object.keys(videoToCreate).length;i++)
	 	{
	 		var attr = Object.keys(videoToCreate)[i];
	 		if(videoToCreate[attr].pics.length==1)
	 			gorilla.coverflowVideos(videoToCreate[attr].divName,videoToCreate[attr].labelName,videoToCreate[attr].container,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);   
		 	else
	 		gorilla.vCarousel(videoToCreate[attr].container,videoToCreate[attr].divName,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);
	 		
	 		gorilla.coverflowVideos(videoToCreate[attr].divName,videoToCreate[attr].labelName,videoToCreate[attr].containerl,videoToCreate[attr].pics,videoToCreate[attr].labels,videoToCreate[attr].actions);   
	 	} 
	}




//--------------------------------------- Vertical Videos Carousel-----------------------

var selectedVideoToShare = -1;
var vCarouselSeries = -1;
gorilla.vCarousel = function(id,divName, pics,labels,actions)
            {
              
                var container = document.getElementById(id);
                container.innerHTML="";
                container.style.height="650px";
                container.style.width="455px";  
                
                var div = document.createElement('div');
                div.id=divName;
                div.style.overflow ="hidden";
                div.style.width = '455px';
                div.style.height = '650px'; //changed from 650
                div.style.position = 'relative';            
                div.carouselPosition = 0;
             
               

                container.appendChild(div);
                
                var sample = new Image();
                sample.onload = function()
                {
                    var strFinal='<div id="vCarousel'+0+'" style="-webkit-transition:-webkit-transform .5s ease-in-out;-webkit-backface-visibility:hidden;">';
					
					//scroller.style.webkitBackfaceVisibility='hidden';
                   
                    for (var d=0; d<pics.length; d++)
                    {
                      if((navigator.userAgent.toLowerCase().search('gt-p1000') > -1))
					  {
						 //alert(1)
						  strFinal+='<center><img id="vCarousel'+0+'im'+d+'" src="'+pics[d]+'" style="width:450px;'+'-webkit-transform:scale(3);margin:0px 0 80px 0; opacity:1;'+
						'-webkit-backface-visibility:hidden;"></center>';
					  }
					  else if((navigator.userAgent.toLowerCase().search("kftt")>-1) || (navigator.userAgent.toLowerCase().search("silk")>-1))
					  {
						 //alert(1)
						  strFinal+='<center><img id="vCarousel'+0+'im'+d+'" src="'+pics[d]+'" style="width:450px;'+'margin:0px 0 80px 0; opacity:1;'+
						'-webkit-backface-visibility:hidden;"></center>';
					  }
                      else
					  {
                          strFinal+='<center><img id="vCarousel'+0+'im'+d+'" src="'+pics[d]+'" style="width:260px;'+(d === 1 ? '-webkit-transform:scale(1.73);margin:131px 0 60px 0; opacity:1;' : 'margin:6px 0;opacity:.8;-webkit-transform:scale(0.8);')+'-webkit-transition:all .5s ease-in-out;-webkit-backface-visibility:hidden;"></center>';
                      }
                        
                    }
                   
					if((navigator.userAgent.toLowerCase().search('gt-p1000') > -1)||(navigator.userAgent.toLowerCase().search("kftt")>-1))
					{
						strFinal+='</div><div id="vCarousel'+0+'banner" style="position:absolute;z-index:2;width:450px;height:86px;top:270px;margin-left:3px;-webkit-transition:all .25s ease-in-out;-webkit-backface-visibility:hidden;background:url(\''+labels[1]+'\');-webkit-background-size:100% 100%;">';                    
					}
					else
					{
					   	strFinal+='</div><div id="vCarousel'+0+'banner" style="position:absolute;z-index:2;width:450px;height:86px;top:164px;margin-left:3px;-webkit-transition:all .25s ease-in-out;-webkit-backface-visibility:hidden;background:url(\''+labels[1]+'\');-webkit-background-size:100% 100%;">';
                    }				
					div.innerHTML = strFinal; 
					
                    for (var i=0;i<pics.length; i++)
                    {
                        var el = div.children[0].children[i].children[0];//document.getElementById('vCarousel'+0+'im'+i);
                        el.addEventListener("touchend",function(e)
                        {                           
                            if (container.noAnim) 
                            {        
                            	actions[selectedVideoToShare]();                            	
                            }
                        });
                    }
                    
                    div.position = 1;
                    selectedVideoToShare = Math.abs(div.position);
                    
                    function transformDivs(mydiv)
                    {
                        
                        if(!(navigator.userAgent.toLowerCase().search('gt-p1000') > -1) && !(navigator.userAgent.toLowerCase().search("kftt")>-1) && !(navigator.userAgent.toLowerCase().search("silk")>-1))
						{
							
							if((div.position>0) && div.position<mydiv.children[0].children.length-1)
                        	{
                        		var divA = mydiv.children[0].children[div.position-1].children[0];//document.getElementById('vCarousel'+0+'im'+(div.position-1));
								var divB = mydiv.children[0].children[div.position].children[0];//document.getElementById('vCarousel'+0+'im'+(div.position+0));
								var divC = mydiv.children[0].children[div.position+1].children[0];//document.getElementById('vCarousel'+0+'im'+(div.position+1));
                        	}else if(div.position==0)
                        	{
                        		var divA = null;
                        		var divB = mydiv.children[0].children[div.position].children[0];//document.getElementById('vCarousel'+0+'im'+(div.position+0));
								var divC = mydiv.children[0].children[div.position+1].children[0]
                        	}else if(div.position==mydiv.children[0].children.length-1)
                        	{
                        		var divA = mydiv.children[0].children[div.position-1].children[0];
                            	var divB = mydiv.children[0].children[div.position].children[0];//document.getElementById('vCarousel'+0+'im'+(div.position+0));
    							var divC = null;
                        	}
							
							if (divA !== null)
							{
								divA.style.webkitTransform = 'scale(0.8)';
								divA.style.opacity = .8;
								divA.style.margin = '6px 0';
							}
							if (divB !== null)
							{
								divB.style.webkitTransform = 'scale(1.73)';
								divB.style.opacity = 1;
								divB.style.margin = '131px 0 60px 0';
							}
							if (divC !== null)
							{
								divC.style.webkitTransform = 'scale(0.8)';
								divC.style.opacity = .8;
								divC.style.margin = '6px 0';
							}
						}
                        //alert(mydiv.children[0].children.length)
                        var banner = mydiv.children[1];
                        function endScale()
                        {
                            banner.removeEventListener('webkitTransitionEnd', endScale, false);
                            banner.style.background = "url('"+labels[div.position]+"')";
                            banner.style.webkitTransform = 'scale(1.0)';
                            banner.style.opacity = 1;
                        }
                        banner.addEventListener('webkitTransitionEnd', endScale, false);
                        banner.style.opacity = 0;
                        banner.style.webkitTransform = 'scale(0.5)';
                    }
                    
                    veltijs.addSwipe(id,
                    function(a)
                    {
                    	if (div.position-1 >= 0)
                    	{
                    		var scroller = a.parentNode.parentNode;//document.getElementById('vCarousel'+0);
                    		if(scroller.id=="vCarousel0"){
	                    		if((navigator.userAgent.toLowerCase().search('gt-p1000') > -1) || (navigator.userAgent.toLowerCase().search("kftt")>-1))
	                            {
	                               scroller.style.webkitTransform = 'translateY('+(div.carouselPosition+=334)+'px)';
	                            }
	                            else{  
	                            	scroller.style.webkitTransform = 'translateY('+(div.carouselPosition+=158)+'px)';
	                            }
	                            div.position--;	                            
	                            selectedVideoToShare = Math.abs(div.position);	                          
	                            transformDivs(div);
                    		}
                    	}
                     },
                     function(a)
                            {
                            	if (div.position+1 <= pics.length-1)
                                {
                                    var scroller = a.parentNode.parentNode;//document.getElementById('vCarousel'+0)//vCarouselSeries);
                                    if(scroller.id=="vCarousel0"){
	                                    if((navigator.userAgent.toLowerCase().search('gt-p1000') > -1) || (navigator.userAgent.toLowerCase().search("kftt")>-1))
										{
											scroller.style.webkitTransform = 'translateY('+(div.carouselPosition-=334 )+'px)';
										}
										else
										{
											//alert(scroller.id);    
											scroller.style.webkitTransform = 'translateY('+(div.carouselPosition-=158)+'px)';
										}
	                                    div.position++;
	                                    selectedVideoToShare = Math.abs(div.position);	                         
	                                    transformDivs(div);
                                    }
                                }
                            },true);     
                  
                };
                sample.src = pics[0];
            }
//----------------------------------end of Vertical Carousel------------------------------------



//----------------------------------Horizontal Carousel------------------------------------

var videoController = 0;

var swipeCoverFlowLeftfn = function(k)
{
	 //left to right ->          
    k.position++;       
    if (k.position === 1) {k.position = 0; }
  	//alert(labels[Math.abs(k.position)]);
    	//GetDiv(labelname).src = labels[Math.abs(k.position)];
    	//document.getElementById(labelname).style.display = "none";
  	document.getElementById(labelname).style.background  = 'url('+labels[Math.abs(k.position)]+')';
	//document.getElementById(labelname).style.display = "block";
	scroller.style.webkitTransform = 'translate('+(k.position*340)+'px,0px)';
	selectedVideoToShare = Math.abs(k.position);
	rotateImgs();
}



gorilla.coverflowVideos = function(divname,labelname,cont, pics, labels, actions)
{
  actions[0];
  var make = (pics.length <=1)?false:true;
  var container = document.getElementById(cont);
  container.innerHTML ="";
  container.style.height="300px";
  container.style.width="600px";  


  
  var klabel = document.createElement("div");
  klabel.id=labelname;//"carouselThumb1";
  klabel.style.display="inline-block";
  klabel.style.position="relative";
  klabel.style.width="340px";
  klabel.style.height="70px";
  klabel.style.top="24px";
  klabel.style.zIndex = "2";
  klabel.style.background = 'url('+labels[0]+')';
  klabel.style.webkitBackgroundSize = "100% 100%";
  klabel.style.marginLeft="130px";
  container.appendChild(klabel);
  
  var k = document.createElement("div");
  
  k.id=divname;
  k.position = 0;
  k.height="300px";
  k.width="600px";
  k.innerHTML = '';
  k.style.position = 'relative';
  k.style.top = '10px';
  k.style.overflow = "hidden";
  k.videoController = ++videoController;
  
      
  container.appendChild(k);
  
  var sample = new Image();
  sample.src = pics[0];
  sample.onload = function()
  {
      var scroller = document.createElement('div');
	  scroller.style.webkitBackfaceVisibility='hidden';
      scroller.id = 'coverflowScroller'+k.videoController;
      scroller.style.cssText = '-webkit-transition:-webkit-transform .4s ease-in-out;width:'+((pics.length+2)*257)+'px;height:'+200+'px';
      if (!(navigator.userAgent.toLowerCase().search('android') > -1) || !(navigator.userAgent.toLowerCase().search("kftt")>-1)) scroller.style.webkitPerspective=700;
      k.appendChild(scroller);
     
      function rotateImgs()
      {
          var divNum = Math.abs(k.position)+1;            
          var div0= document.getElementById('coverflow'+k.videoController+'pic'+Math.abs(divNum-1));
          var div1= document.getElementById('coverflow'+k.videoController+'pic'+Math.abs(divNum));
          var div2= document.getElementById('coverflow'+k.videoController+'pic'+Math.abs(divNum+1));
      
          if(div0.nodeName=="DIV")
          {       
              div2.style.webkitTransform ="rotateY(65deg) scale(0.8)";
              div1.style.webkitTransform ="rotateY(0deg) scale(1.0)";
              div2.style.webkitTransformOrigin ="0% 50%";
          }else if (div2.nodeName =="DIV")
          {
              div0.style.webkitTransform ="rotateY(-65deg) scale(0.8)";
              div1.style.webkitTransform ="rotateY(0deg) scale(1.0)";
              div0.style.webkitTransformOrigin ="100% 50%";
          }else{
              div0.style.webkitTransform ="rotateY(-45deg) scale(0.8)";
              div1.style.webkitTransform ="rotateY(0deg) scale(1.0)";
              div2.style.webkitTransform ="rotateY(45deg) scale(0.8)";
              div2.style.webkitTransformOrigin ="0% 50%";
              div0.style.webkitTransformOrigin ="100% 50%";
          }
          
      }
      
      if(make){
    	  
    
   if(pics.length !=1)
   {
	   veltijs.addSwipe(scroller.id, 
	    function()
	      {
	          
	    	
	    	  //left to right ->          
	          k.position++;       
	          if (k.position === 1) {k.position = 0; }
		    	//alert(labels[Math.abs(k.position)]);
	          	//GetDiv(labelname).src = labels[Math.abs(k.position)];
	          	//document.getElementById(labelname).style.display = "none";
		    	document.getElementById(labelname).style.background  = 'url('+labels[Math.abs(k.position)]+')';
			//document.getElementById(labelname).style.display = "block";
			scroller.style.webkitTransform = 'translate('+(k.position*340)+'px,0px)';
	      	selectedVideoToShare = Math.abs(k.position);
	      	rotateImgs();
	      
	      },
	      function()
	      {
	          //right to left <-  
	    	  if(k.position-1 != -pics.length)
	          {
	        	 k.position--; 
		          if (k.position === pics.length) k.position = -pics.length+1;
		          //GetDiv(labelname).src = labels[Math.abs(k.position)];
		          //document.getElementById(labelname).style.display = "none";
					document.getElementById(labelname).style.background  = 'url('+labels[Math.abs(k.position)]+')';
					//document.getElementById(labelname).style.display = "block";
					//document.getElementById(labelname).onclick =function(){alert(labels[Math.abs(k.position)])}
					scroller.style.webkitTransform = 'translate('+(k.position*340)+'px,0px)';
		          selectedVideoToShare = Math.abs(k.position);
		          rotateImgs();
	          }
	      });      
	      }
      }else
      {
    	  //This is when there is only one element
    	  scroller.addEventListener("touchend", function(e){
    		 
    	  if (!e) var event = window.event;
			e.cancelBubble = true;
			e.preventDefault();
			if (e.stopPropagation) e.stopPropagation();	  
    	  });
    	  
    	  scroller.addEventListener("touchstart", function(e){
     		 
        	  if (!e) var event = window.event;
    			e.cancelBubble = true;
    			e.preventDefault();
    			if (e.stopPropagation) e.stopPropagation();	  
        	  });
    	  scroller.addEventListener("touchmove", function(e){
      		 
        	  if (!e) var event = window.event;
    			e.cancelBubble = true;
    			e.preventDefault();
    			if (e.stopPropagation) e.stopPropagation();	  
        	  });
      }
      
      var coverflow0pic0 = document.createElement('div');     
      coverflow0pic0.id = 'coverflow'+k.videoController+'pic0';
      coverflow0pic0.style.cssText = 'float:left;width:130px;HEIGHT:1px';
      scroller.appendChild(coverflow0pic0);
      for(var d =0;d<pics.length;d++)
      {
          
    	  var piccie = document.createElement('img');
          piccie.id = 'coverflow'+k.videoController+'pic'+(parseInt(d)+1);
          piccie.src = pics[d];           
          piccie.fn = actions[d];
          piccie.style.cssText = '-webkit-box-sizing:border-box;border:3px solid #fff;float:left;width:340px;-webkit-transition:-webkit-transform .3s ease-in-out ';
          piccie.ontouchend = function(e)
          {
        	  
        	  if(pics.length == 1 ){
        		  this.fn();
        	  }
        	  else{
        		  if (scroller.noAnim){
              	  this.fn();    
        		  }
        	  }
              
          };
          scroller.appendChild(piccie);
      };
      
      if(pics.length >2){
          k.position = -1;
          scroller.style.webkitTransform = 'translate('+(k.position*340)+'px, 0px)';
          document.getElementById(labelname).style.background  = 'url('+labels[Math.abs(k.position)]+')';  
        //GetDiv(labelname).src = labels[Math.abs(k.position)];
          selectedVideoToShare = Math.abs(k.position);
          rotateImgs(); 
      }else if(pics.length >1){
          rotateImgs(); 
      }
      var coverflow0picEnd = document.createElement('div');
      coverflow0picEnd.id = 'coverflow'+k.videoController+'pic'+(pics.length+1);
      coverflow0picEnd.style.cssText = 'float:left;width:130px;HEIGHT:1px';
      scroller.appendChild(coverflow0picEnd);
  };
};



//----------------------------------end of Horizontal Carousel------------------------------------





// ------------------------|END VIDEO PAGE|----------------------------//




//---------------------|Fun With Gorilla Page|-------------------------//

gorilla.playMP3 = function(el)
{
	var button_id = el.id;
	var audio = document.getElementById("gorilla_ringtones");
	
	if (button_id == "hootsmp3_pr")
	{		
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_hoots');
        
		audio.src='assets/funWithGorillaPage/audio/hoots.mp3';		
	}
	else if (button_id == "victormp3_pr")
	{		
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_victor');
		
        audio.src='assets/funWithGorillaPage/audio/victor.mp3';
	}
	else if (button_id == "clubmp3_pr")
	{	
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_club');
		
        audio.src='assets/funWithGorillaPage/audio/club.mp3';
	}
	else if (button_id == "congomp3_pr")
	{		
		//analytics              
        veltijs.pushCustomEventAnalytics('ringtone_congo');
        
		audio.src='assets/funWithGorillaPage/audio/congo.mp3';
	}
	
	setTimeout(function(){
		audio.play();
	} ,10)
	
	
};


gorilla.ringTonechoise="";
gorilla.sendRingTone= function(btn)
{
	 gorilla.ringTonechoise = btn.id;		
	 gorilla.showPageById('GorillaEmail_page');
};



//---------------------------------|Email Page|------------------------------------------

gorilla.clearEmaillabel = function()
{
	//every time enter page clear Email label
	var validation_msg = document.getElementById('validation_msg');
	validation_msg.innerHTML="";   
	
	var emailField = document.getElementById('gorilla_email_ringtone_val');
	emailField.value="";
};

gorilla.sendFileViaEmail = function(w)
{    
	var validation_msg = document.getElementById('validation_msg');
    var email = document.getElementById('gorilla_email_ringtone_val').value;
       
    if (email == '')
    {
    	//fill Email label
    	validation_msg.innerHTML = "Please enter an email address.";
    }
    else
    {	 
	    //check email if it's correct
		var flagEmail = gorilla.isValidEmail(email);
		
		//if it's not correct
		if (flagEmail == false)
		{
			validation_msg.innerHTML = "Please enter a valid email address.";
		}
		else
		{
		  var actualFile = '';    
		  if(gorilla.ringTonechoise != '')
		  {
			  switch (gorilla.ringTonechoise)
			  {
				  case 'hootsmp3' :
				  actualFile = 'HootsGrunts.mp3';
				  break;
				  
				  case 'hootsm4r':
				  actualFile = 'HootsGrunts.m4r';
				  break;
				  
				  case 'victorm4r':
				  actualFile = 'VictorsGmix.m4r';
				  break;
				  
				  case 'victormp3':
				  actualFile = 'VictorsGmix.mp3';
				  break;
				  
				  case 'clubmp3':
				  actualFile = 'ClubGorilla.mp3';
				  break;
				  
				  case 'clubm4r':
				  actualFile = 'ClubGorilla.m4r';
				  break;
                                        
                  case 'congomp3':
                  actualFile = 'CallsfortheCongo.mp3';
                  break;
                  
                  case 'congom4r':
                  actualFile = 'CallsfortheCongo.m4r';
                  break;                                                					  
			  }				  
			  //subject
			  var subject = "Ringtone from a Friend";
			  
			  //body
			  var bodyTemplate = "A friend would like to share this ringtone with you from the Corning Gorilla Glass website";
			  
			  var data =
			  {
				  'formMap':gorilla.services.sendfile_viaemail_service,
				  'instanceId':'AEAAF8AB-490A-BD17-8562-DD8615B3B949',
				  'system_send_name':'CorningGorillaGlass',
				  'system_send_email':'GorillaGlass@vendor.com',
				  'showFriendsName':'false',
				  'showSendersEmail':'false',
				  'showSendersName':'false',
				  'friendEmail':email,
				  'optOutURL':'',
				  'attachementFileName':actualFile,
				  'bodyTemplate':bodyTemplate,
				  'subject':subject
			  };
			  
			  veltijs.proxy.sendRequest(gorilla.services.sendfile_viaemail_host, 'GET', 'true', data, gorilla.responseSendviaEmail);
		  }  
		}//end correct email
  
    }//end if
};


//function to check valid email address
gorilla.isValidEmail = function(strEmail)
{    
	var validRegExp = new RegExp('[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})','i');
	
	var contains_space = strEmail.indexOf(' ');
	//alert(contains_space);
	if (contains_space >= 0){		
		return false;
	}
	
	//search email text for regular exp matches
	if (validRegExp.test(strEmail)){
		return true;
	}
	else{
		return false;
	}	
};


gorilla.responseSendviaEmail = function(req)
{
	var validation_msg = document.getElementById('validation_msg');
	var email = document.getElementById('gorilla_email_ringtone_val');
	
	//alert(req.responseText);
	if (req.responseText == "")
    {
        //fill Email label
		validation_msg.innerHTML = "Thank you. Your email has been sent.";
		
		//download analytics
		veltijs.pushCustomEventAnalytics(gorilla.ringTonechoise);
		
		//clear textbox
		email.value="";
    }
	else
	{
		//fill Email label
		validation_msg.innerHTML = "You have exceeded the limit of 5 emails/day. Please try again later.";
		
        //clear textbox
		email.value="";
	}
};

//---------------------|Email Page|-------------------------//


//---------------------- Praise Submit Button ----------------------------
gorilla.praise_commit = function()
{	
    var pname = document.getElementById('praise_name').value;
    var pcomments = document.getElementById('praise_comments').value; 
    
    if ((pname != "") && (pcomments != ""))
    {
    	//loading indicator
    	gorilla.switchLoadingIndicator(true);
    	
    	var data ={'name':pname, 'comment':pcomments};		    
        veltijs.proxy.sendRequest(gorilla.services.submit_service, 'GET', true, data, gorilla.commit_praise_res);	
        
        //every time button pressed clear fields
    	var nameFieldCons = document.getElementById('praise_name');
    	var commentsFieldCons = document.getElementById('praise_comments');
    	var validationField = document.getElementById('validation_praise');
    	nameFieldCons.value="";
    	commentsFieldCons.value="";
    	validationField.innerHTML="";
    }
    else
    {
    	document.getElementById('validation_praise').innerHTML="Please insert your name and add your comment.";
    }
      
    
};

//---------------------- Praise consumers Submit Button ----------------------------
gorilla.praise_commit_consumers = function()
{
	var pnameCons = document.getElementById('praise_name_consumers').value;
    var pcommentsCons = document.getElementById('praise_comments_consumers').value;
	
    if ((pnameCons != "") && (pcommentsCons != ""))
    {
		//loading indicator
		gorilla.switchLoadingIndicator(true);
	       
	  	var data ={'name':pnameCons, 'comment':pcommentsCons};	
	    veltijs.proxy.sendRequest(gorilla.services.submit_service, 'GET', true, data, gorilla.commit_praise_res);	      		         
	    
	    //every time button pressed clear fields
		var nameFieldCons = document.getElementById('praise_name_consumers');
		var commentsFieldCons = document.getElementById('praise_comments_consumers');
		var validationAllField = document.getElementById('validation_allpraise');
		nameFieldCons.value="";
		commentsFieldCons.value="";
		validationAllField.innerHTML="";
    }
    else
    {
    	document.getElementById('validation_allpraise').innerHTML="Please insert your name and add your comment.";
    }
              
};

gorilla.commit_praise_res = function(req)
{
    var res = JSON.parse(req.responseText);   
    if(res.submited)
    {
    	gorilla.switchLoadingIndicator(false);
    	
        //show thank you page
    	gorilla.showPageById("PraiseThankYou_page");    	
    }
    else
    {    	
        //error message
        res.message;
    }
};




//---------------------|Sharing Functionality|-------------------------//
gorilla.socialSharing = function(btn)
{
	//alert(btn.id);
	var button_id = btn.id;	
	var func_id = button_id.split("_")[1];
	
	switch (func_id)
	{
		case "fb":
			gorilla.FacebookShare(button_id);
			break;
			
		case "tw":
			gorilla.TwitterShare(button_id);
			break;
		
		case "sms":
			gorilla.smsPrevPage = location.hash.replace('#','');			
			gorilla.smsButtonId = button_id;
			gorilla.showPageById('GorillaSMS_page');
			break;
		
		case "email":
			gorilla.shareViaEmail(button_id);
			break;
	}	
};


//--------------------------- FACEBOOK ----------------------------

gorilla.FacebookShare = function(idButton)
{   
    var page = "";
    
    switch (idButton)
    {
        case 'home_fb' :
            page = "page1";
        break;
    
        case 'products_fb' :
            page = "page2";
        break;
        
        case 'fun_fb' :
            page = "page26";
        break;
        
        case 'news_fb' :
            page = "page18";
        break;
        
        case 'faq_fb' :
            page = "page41";
        break;
        
        case 'productsfull_fb' :
            page = "page3";
        break;
        
        case 'specificnews_fb' :
            page = "page19";
        break;
        
        
        //innovating
        case 'over_fb' :
            page = "page21";
        break;
        
        case 'chara_fb' :
            page = "page22";
        break;
        
        case 'custo_fb' :
            page = "page23";
        break;
        
        case 'appli_fb' :
            page = "page24";
        break;  

        case 'lite_fb' :
            page = "page25";
        break;

        
        //videos        
        case 'victor_fb' :       
        	//alert(selectedVideoToShare);
            if (selectedVideoToShare === 0)//channel surfing 
            {             	
                page = "page33";
                //analytics              
                veltijs.pushCustomEventAnalytics('victor1_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "Channel Surfing video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)//cooking up tomorrow's kitchen
            {             	
                page = "page34";
                //analytics
                veltijs.pushCustomEventAnalytics('victor2_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "Cooking Up Tomorrow's Kitchen video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 2)//king of the office
            {             	
                page = "page35";
                //analytics
                veltijs.pushCustomEventAnalytics('victor3_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "King of the Office video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'demos_fb' :        	
            if (selectedVideoToShare === 0)// How Corning Tests Gorilla Glass
            {                 
                page = "page51";
                //analytics
                veltijs.pushCustomEventAnalytics('demos1_fb');
                                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "How Corning Tests Gorilla Glass", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'saying_fb' :        
            if (selectedVideoToShare === 0)//gorilla tough 
            {              
                page = "page38";
                //analytics
                veltijs.pushCustomEventAnalytics('saying1_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "Gorilla Tough video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)//Microsoft Puts Corning Gorilla Glass 2 to the Test
            {                    
                page = "page50";
                //analytics
                veltijs.pushCustomEventAnalytics('saying2_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos page"];
                //fiveml._invokeTracking(args);              
            }
        break;
        case 'brought_fb' :        
            if (selectedVideoToShare === 0)//a day made of
            {              
                page = "page39";
                //analytics                
                veltijs.pushCustomEventAnalytics('brought1_fb');
                
                //var args = ["trackEvent", "GorillaVideos", "FacebookShares", "A Day Made of Glass video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)//Corning Gorilla Glass for Seamless Elegant Design
            {              
            	page = "page49";
                //analytics
            	veltijs.pushCustomEventAnalytics('brought2_fb');
            	
            	//var args = ["trackEvent", "GorillaVideos", "FacebookShares", "Corning Gorilla Glass for Seamless Elegant Design", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
            {
                page = "page52";
                // analytics
                veltijs.pushCustomEventAnalytics('brought3_fb');
                                
                //var args = [ "trackEvent", "GorillaVideos", "FacebookShares", "Changing the Way We Think about Glass", "Videos page"];
            	//fiveml._invokeTracking(args);
            }
            
        break;  
        
        
        //wallpapers
        case 'wallpaper_fb' :
        	//alert(currentId);
            if (currentId == 'coverflow0pic1' )//fatsa
            {
                page = "page31";
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic1_fb');
                
                //var args = ["trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla face", "Wallpaper - Gorilla face"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic2')//filaei kato
            {
                page = "page29";
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic2_fb');
                
                //var args = ["trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Kiss", "Wallpaper - Gorilla Kiss"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic3')//kornizes
            {
                page = "page30";
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic3_fb');
                
                //var args = ["trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Frame", "Wallpaper - Gorilla Frame"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic4')//bananes
            {
                page = "page28";
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic4_fb');
                
                //var args = ["trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Banana", "Wallpaper - Gorilla Banana"];
                //fiveml._invokeTracking(args);
            }else if (currentId == 'coverflow0pic5')// Fossey
            {
                page = "page48";                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic5_fb');
                
                //var args = ["trackEvent", "SpecificWallpaper", "FacebookShares", "Wallpaper - Gorilla Fossey", "Wallpaper - Gorilla Fossey"];
                //fiveml._invokeTracking(args);
             }
        break;      


        //products
        case 'acer_fb' :
            page = "page4";
        break;  
        
        case 'dell_fb' :
            page = "page6";
        break;  
        
        case 'htc_fb' :
            page = "page7";
        break;  
        
        case 'kyo_fb' :
            page = "page8";
        break;  
        
        case 'asus_fb' :
            page = "page5";
        break;  
        
        case 'lg_fb' :
            page = "page10";
        break;  
        
        case 'moto_fb' :
            page = "page11";
        break;  
        
        case 'moti_fb' :
            page = "page12";
        break;  
        
        case 'nec_fb' :
            page = "page13";
        break;  
        
        case 'nokia_fb' :
            page = "page14";
        break;  
        
        case 'sam_fb' :
            page = "page15";
        break;  
        
        case 'sk_fb' :
            page = "page16";
        break;  
        
        case 'sony_fb' :
            page = "page17";
        break;  

        case 'lenovo_fb' :
            page = "page9";
        break;
        
        case 'hyundai_fb' :
            page = "page40";
        break;
          
        case 'praise_fb':
            page = "page44";
        break;

        case 'allpraise_fb':
            page = "page44";
        break;
            
        case 'sur40_fb':
            page = "page46";
        break;
        
        case 'hp_fb':
            page = "page45";
        break;  
          
        case 'sonim_fb':
            page = "page47";
        break; 
        
        case 'lumigon_fb':
            page = "page53";
        break;
    }

    var url = gorilla.services.facebook_url + page + ((document.location.href.search('corninggorillaglass') > -1)? "&redirect=true":"&noredirect=true");
  
    setTimeout(function() 
    {
    	gorilla.openExternalLink('http://www.facebook.com/sharer.php?u='+escape(url)); 
    	//var newwin = window.open('http://www.facebook.com/sharer.php?u='+escape(url)); 
			//if(!newwin)
			//	showPopupBlocker();
    }, 500);               
};



//--------------------------- TWITTER ----------------------------
gorilla.TwitterShare = function(idButton)
{   
    var text = "";
    
    switch (idButton)
    {
        case 'home_tw' :
            text = "Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out @ www.corninggorillaglass.com"; 
        break;
    
        case 'products_tw' :
            text = "Look for Corning® Gorilla® Glass on 425 product models. Is it on yours? Check it out at http://bit.ly/oJ4pxI"; 
        break;
    
        case 'fun_tw' :
            text = "Download your own awesome Victor the Gorilla wallpaper or video @ http://www.corninggorillaglass.com/fun-with-gorilla"; 
        break;
    
        case 'news_tw' :
            text = "Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events"; 
        break;
        
        case 'specificnews_tw' :
            text = "Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events"; 
        break; 
    
        case 'faq_tw' :
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/faqs"; 
        break; 
    
        case 'productsfull_tw' :
            text = "Look for Corning® Gorilla® Glass on 30 major brands. Is it on yours? Check it out @ http://bit.ly/qHKBVK"; 
        break; 
    
        //innovating
        case 'over_tw' ://OVERVIEW
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/mS71FT";
        break;
        
        case 'chara_tw' ://CHARACTERISTICS
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/qzZPVO";
        break;
        
        case 'custo_tw' ://CUSTOMIZATION
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/nXeJZG";
        break;
        
        case 'appli_tw' ://APPLICATIONS
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/rnebhj";
        break;  

        case 'lite_tw' ://LITERATURE
            text = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://bit.ly/q1TIBV";
        break;
        
        
        //videos        
        case 'victor_tw' :
            if (selectedVideoToShare === 0)//channel surfing 
            { 
                text = "Watch Corning® Gorilla® Glass Channel Surfing Video @ http://youtu.be/5Qxknat5o_Q"; 
                //analytics
                veltijs.pushCustomEventAnalytics('victor1_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "Channel Surfing video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)//cooking up tomorrow's kitchen
            { 
                text = "Watch Corning® Gorilla® Glass COOKING UP TOMORROWS KITCHEN Video @ http://youtu.be/ZUQEFL45Iko";
                //analytics
                veltijs.pushCustomEventAnalytics('victor2_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "Cooking Up Tomorrow's Kitchen video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 2)//king of the office
            { 
                text = "Watch Corning® Gorilla® Glass KING OF THE OFFICE Video @ http://youtu.be/X0MhSIFgW9U";
                //analytics
                veltijs.pushCustomEventAnalytics('victor3_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "King of the Office video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'demos_tw' :
            if (selectedVideoToShare === 0)//How Corning Tests Gorilla Glass
            { 
                text = "Watch Corning® Gorilla® Glass HOW CORNING TESTS Video @ http://www.corninggorillaglass.com/videos";
                //analytics
                veltijs.pushCustomEventAnalytics('demos1_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "How Corning Tests Gorilla Glass", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'saying_tw' :
            if (selectedVideoToShare === 0)// gorilla tough
            {
                text = "Watch Corning® Gorilla® Glass GORILLA TOUGH Video @ http://www.corninggorillaglass.com/videos";
                //analytics
                veltijs.pushCustomEventAnalytics('saying1_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "Gorilla Tough video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)// Microsoft Puts Corning Gorilla Glass 2 to the Test
            {              
                text = "Watch Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos";
                //analytics
                veltijs.pushCustomEventAnalytics('saying2_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'brought_tw' :
            if (selectedVideoToShare === 0)// a day made of
            {
                text = "Watch Corning® Gorilla® Glass A DAY MADE OF GLASS Video @ http://youtu.be/6Cf7IL_eZ38";
                //analytics
                veltijs.pushCustomEventAnalytics('brought1_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "A Day Made of Glass video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)// Corning Gorilla Glass for Seamless Elegant Design
            {
                text = "Watch Corning® Gorilla® Glass MODERN DESIGN Video @ http://www.corninggorillaglass.com/videos";
                //analytics
                veltijs.pushCustomEventAnalytics('brought2_tw');
                
                //var args = ["trackEvent", "GorillaVideos", "TwitterShares", "Corning Gorilla Glass for Seamless Elegant Design", "Videos page"];
                //fiveml._invokeTracking(args);              
            }
            else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
            {
                text = "Watch Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos";    			
            	//analytics
                veltijs.pushCustomEventAnalytics('brought3_tw');
                
                //var args = [ "trackEvent", "GorillaVideos", "TwitterShares", "Changing the Way We Think about Glass", "Videos page"];
            	//fiveml._invokeTracking(args);
            }   
        break;  
        
        
        //wallpapers
        case 'wallpaper_tw' :
            if (currentId == 'coverflow0pic1' )//fatsa
            {
                text = "Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/nRgqg1"; 
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic1_tw');
                
                //var args = ["trackEvent", "SpecificWallpaper", "TwitterShares", "Wallpaper - Gorilla face", "Wallpaper - Gorilla face"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic2')//filaei kato
            {
                text = "Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/nLGTaS"; 
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic2_tw');
                
                //var args = ["trackEvent", "SpecificWallpaper", "TwitterShares", "Wallpaper - Gorilla Kiss", "Wallpaper - Gorilla Kiss"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic3')//kornizes
            {
                text = "Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/pw15cT"; 
                                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic3_tw');
                
                //var args = ["trackEvent", "SpecificWallpaper", "TwitterShares", "Wallpaper - Gorilla Frame", "Wallpaper - Gorilla Frame"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic4')//bananes
            {
                text = "Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://bit.ly/o43IGW"; 
                                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic4_tw');
                
                //var args = ["trackEvent", "SpecificWallpaper", "TwitterShares", "Wallpaper - Gorilla Banana", "Wallpaper - Gorilla Banana"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic5')//Fossey
            {
                text = "Screens are better with gorillas from Corning® Gorilla® Glass Wallpapers. Download yours @ http://meds.mblgrt.com/ch/36463/Fossy_Wallpapers";
                                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic5_tw');
                
                //var args = ["trackEvent", "SpecificWallpaper", "TwitterShares", "Wallpaper - Gorilla Fossey", "Wallpaper - Gorilla Fossey"];
                //fiveml._invokeTracking(args);
            }
        break;      

            
        //products
        case 'acer_tw' :
            text = "Check out Acer's products featuring Corning® Gorilla® Glass @ http://bit.ly/kIxpI7"; 
        break;  
        
        case 'dell_tw' :
            text = "Check out Dell products featuring Corning® Gorilla® Glass @ http://bit.ly/qdbLQD"; 
        break;  
        
        case 'htc_tw' :
            text = "Check out HTC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/htc"; 
        break;  
        
        case 'kyo_tw' :
            text = "Check out Kyocera products featuring Corning® Gorilla® Glass @ http://bit.ly/nOL9U2"; 
        break;  
        
        case 'asus_tw' :
            text = "Check out AsusA® products featuring Corning® Gorilla® Glass @ http://bit.ly/r3XOwk"; 
        break;  
        
        case 'lg_tw' :
            text = "Check out LG products featuring Corning® Gorilla® Glass @ http://bit.ly/qvzyLL "; 
        break;  
        
        case 'moto_tw' :
            text = "Check out Motorola products featuring Corning® Gorilla® Glass @ http://bit.ly/o3lo1s"; 
        break;  
        
        case 'moti_tw' :
            text = "Check out Motion Computing  products featuring Corning® Gorilla® Glass @ http://bit.ly/riTX1E"; 
        break;  
        
        case 'nec_tw' :
            text = "Check out NEC products featuring Corning® Gorilla® Glass @ http://bit.ly/puEQeR"; 
        break;  
        
        case 'nokia_tw' :
            text = "Check out Nokia products featuring Corning® Gorilla® Glass at http://bit.ly/ouBGWW"; 
        break;  
        
        case 'sam_tw' :
            text = "Check out Samsung products featuring Corning® Gorilla® Glass @ http://bit.ly/qEoNkM"; 
        break;  
        
        case 'sk_tw' :
            text = "Check out SK Telesys products featuring Corning® Gorilla® Glass @ http://bit.ly/qMcBtP"; 
        break;  
        
        case 'sony_tw' :
            text = "Check out Sony products featuring Corning® Gorilla® Glass  @ http://www.corninggorillaglass.com/SonyBRAVIA/for_bravia.html"; 
        break;

        case 'lenovo_tw' :
            text = "Check out Lenovo products featuring Corning® Gorilla® Glass @ http://bit.ly/px9QUK"; 
        break;  

        case 'hyundai_tw' :
            text = "Check out Hyundai products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/products-with-gorilla/hyundai"; 
        break;  
          
        case 'praise_tw':
            text = "Want to show your praise for Corning® Gorilla® Glass? Check us out at http://www.corninggorillaglass.com/praise-for-gorilla";
        break;

        case 'allpraise_tw':
            text = "Want to show your praise for Corning® Gorilla® Glass? Check us out at http://www.corninggorillaglass.com/praise-for-gorilla";
        break;
          
        case 'sur40_tw':
            text = "Check out Samsung products featuring Corning® Gorilla® Glass @ http://bit.ly/Hx4e9j";
        break;  
        
        case 'hp_tw':
            text = "Check out HP products featuring Corning® Gorilla® Glass @ http://bit.ly/I5bn2e";
        break;
          
        case 'sonim_tw':
            text = "Check out Sonim products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/products-with-gorilla/sonim";
        break;
        
        case 'lumigon_tw':
            text = "Check out Lumigon products featuring Corning® Gorilla® Glass @ http://www.corninggorillaglass.com/product/lumigon";
        break;
    }
    
    window.setTimeout(function() {
    	gorilla.openExternalLink('http://twitter.com/share?url=&text='+encodeURI(text));
	}, 30);   

};



//---------------------------------------  Share Via Email  ----------------------------------------------------------
gorilla.shareViaEmail = function(button_id)
{
    var subject = "";
    var body = "";
     
    switch (button_id)
    {
        case 'home_email' :
            body = "Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out http://www.corninggorillaglass.com";
            subject = "Corning® Gorilla® Glass";
        break;
    
        case 'fun_email' :
            body = "Download your own awesome Victor the Gorilla wallpaper or ringtone @ http://www.corninggorillaglass.com/fun-with-gorilla";
            subject = "Corning® Gorilla® Glass - Wallpapers";
        break;
        
        case 'news_email':
            body = "Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events";
            subject = "Corning® Gorilla® Glass";
        break;

        case 'specificnews_email':
            body = "Check out Corning® Gorilla® Glass in the News @ http://www.corninggorillaglass.com/news-events";
            subject = "Corning® Gorilla® Glass";
        break;
        
        case 'products_email':
            body = "Corning® Gorilla® Glass is changing the way the world thinks about glass. Check it out @ http://www.corninggorillaglass.com/products-with-gorilla";
            subject = "Tough Yet Beautiful";
        break; 
        
        case 'productsfull_email':
            body = "Look for Corning® Gorilla® Glass on 425 product models. Is it on yours? Check it out @ http://www.corninggorillaglass.com/products-with-gorilla/full-products-list";
            subject = "Tough Yet Beautiful";
        break;  

        case 'faq_email':
            body = "Corning® Gorilla® Glass is changing the way the world thinks about glass. Have questions? Check us out at http://www.corninggorillaglass.com/faqs";
            subject = "Corning® Gorilla® Glass";
        break; 
        
        //innovating
        case 'over_email' ://OVERVIEW
            body = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/innovating-with-gorilla";
            subject = "Corning® Gorilla® Glass";
        break;
        
        case 'chara_email' ://CHARACTERISTICS
            body = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/characteristics";
            subject = "Corning® Gorilla® Glass";
        break;
        
        case 'custo_email' ://CUSTOMIZATION
            body = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/customization";
            subject = "Corning® Gorilla® Glass";
        break;
        
        case 'appli_email' ://APPLICATIONS
            body = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/applications";
            subject = "Corning® Gorilla® Glass";
        break;  

        case 'lite_email' ://LITERATURE
            body = "Corning® Gorilla® Glass is creating exciting new possibilities for electronic devices check it out @ http://www.corninggorillaglass.com/literature";
            subject = "Corning® Gorilla® Glass";
        break;
        
        
        //videos        
        case 'victor_email' :
            if (selectedVideoToShare === 0)//channel surfing 
            { 
                body = "Check out the 'Channel Surfing' video Corning® Gorilla® Glass Video @ http://youtu.be/5Qxknat5o_Q";
                subject = "Corning® Gorilla® Glass CHANNEL SURFING";
                //analytics
                veltijs.pushCustomEventAnalytics('victor1_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "Channel Surfing video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)//cooking up tomorrow's kitchen
            { 
                body = "Check out the 'Cooking up tomorrow's kitchen' video by Corning® Gorilla® Glass Video @ http://youtu.be/ZUQEFL45Iko";
                subject = "Corning® Gorilla® Glass TOMORROWS KITCHEN";
                //analytics
                veltijs.pushCustomEventAnalytics('victor2_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "Cooking Up Tomorrow's Kitchen video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 2)//king of the office
            { 
                body = "Check out the 'King of the Office' video by CorningA® GorillaA® Glass Video @ http://youtu.be/X0MhSIFgW9U";
                subject = "Corning® Gorilla® Glass KING OF THE OFFICE";
                //analytics
                veltijs.pushCustomEventAnalytics('victor3_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "King of the Office video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'demos_email' :           	
            if (selectedVideoToShare === 0)// How Corning Tests Gorilla Glass
            {
                body = "Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos";
                subject = "Corning® Gorilla® Glass HOW CORNING TESTS";
                //analytics
                veltijs.pushCustomEventAnalytics('demos1_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "How Corning Tests Gorilla Glass", "Videos page"];
                //fiveml._invokeTracking(args);
            }
        break;
        case 'saying_email' :
            if (selectedVideoToShare === 0)// gorilla tough
            {
                body = "Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos";
                subject = "Corning® Gorilla® Glass GORILLA TOUGH";
                //analytics
                veltijs.pushCustomEventAnalytics('saying1_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "Gorilla Tough video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)// Microsoft Puts Corning Gorilla Glass 2 to the Test
            {
            	body = "Check out this Corning® Gorilla® Glass  Video @ http://www.corninggorillaglass.com/videos";
            	subject = "Corning® Gorilla® Glass GORILLA 2 AND MICROSOFT";
            	// analytics
            	veltijs.pushCustomEventAnalytics('saying2_email');
            	
            	//var args = [ "trackEvent", "GorillaVideos", "EmailShares", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos page" ];
            	//fiveml._invokeTracking(args);
            }
        break;
        case 'brought_email' :
            if (selectedVideoToShare === 0)// a day made of
            {
                body = "Check out the video 'A Day Made of Glass' by Corning® Gorilla® Glass @ http://youtu.be/6Cf7IL_eZ38";
                subject = "Corning® Gorilla® Glass A DAY MADE OF GLASS";
                //analytics
                veltijs.pushCustomEventAnalytics('brought1_email');
                
                //var args = ["trackEvent", "GorillaVideos", "EmailShares", "A Day Made of Glass video", "Videos page"];
                //fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 1)// Corning Gorilla Glass for Seamless Elegant Design
            {
                body = "Check out this Corning® Gorilla® Glass  Video @ http://www.corninggorillaglass.com/videos";
            	subject = "Corning® Gorilla® Glass MODERN DESIGN";
            	// analytics
            	veltijs.pushCustomEventAnalytics('brought2_email');
            	
            	//var args = [ "trackEvent", "GorillaVideos", "EmailShares", "Corning Gorilla Glass for Seamless Elegant Design", "Videos page" ];
            	//fiveml._invokeTracking(args);
            }
            else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
            {
                body = "Check out this Corning® Gorilla® Glass Video @ http://www.corninggorillaglass.com/videos";
                subject = "Corning® Gorilla® Glass CHANGING THE WAY WE THINK ABOUT GLASS";
            	// analytics
                veltijs.pushCustomEventAnalytics('brought3_email');
                
                //var args = [ "trackEvent", "GorillaVideos", "EmailShares", "Changing the Way We Think about Glass", "Videos page"];
            	//fiveml._invokeTracking(args);
            }
        break;  
        
        
        //wallpapers
        case 'wallpaper_email':
            if (currentId == 'coverflow0pic1' )
            {
                body = "Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/ch/29645/Corning_GorillaGlass_Wall";
                subject = "Corning® Gorilla® Glass";
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic1_email');
                
                //var args = ["trackEvent", "SpecificWallpaper", "EmailShares", "Wallpaper - Gorilla face", "Wallpaper - Gorilla face"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic2')
            {
                body = "Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/ch/29621/Corning_GorillaGlass_Wall";
                subject = "Corning® Gorilla® Glass";
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic2_email');
                
                //var args = ["trackEvent", "SpecificWallpaper", "EmailShares", "Wallpaper - Gorilla Kiss", "Wallpaper - Gorilla Kiss"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic3')
            {
                body = "Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/ch/29629/Corning_GorillaGlass_Wall";
                subject = "Corning® Gorilla® Glass";
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic3_email');
                
                //var args = ["trackEvent", "SpecificWallpaper", "EmailShares", "Wallpaper - Gorilla Frame", "Wallpaper - Gorilla Frame"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic4')
            {
                body = "Download your own awesome Victor the Gorilla wallpaper @ http://meds.mblgrt.com/ch/29637/Corning_GorillaGlass_Wall";
                subject = "Corning® Gorilla® Glass";
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic4_email');
                
                //var args = ["trackEvent", "SpecificWallpaper", "EmailShares", "Wallpaper - Gorilla Banana", "Wallpaper - Gorilla Banana"];
                //fiveml._invokeTracking(args);
            }else if(currentId == 'coverflow0pic5')
            {
                body = "Download your own awesome Fossey wallpaper @ http://meds.mblgrt.com/ch/36463/Fossy_Wallpapers";
                subject = "Corning® Gorilla® Glass";
                
                //analytics
                veltijs.pushCustomEventAnalytics('coverflow0pic5_email');
        	        	
                //var args = ["trackEvent", "SpecificWallpaper", "EmailShares", "Wallpaper - Gorilla Fossey", "Wallpaper - Gorilla Fossey"];
                //fiveml._invokeTracking(args);
            }
        break;
        
        
        //products
        case 'acer_email' :
            body = "Check out Acer's products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/acer";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'dell_email' :
            body = "Check out Dell products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/dell";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'htc_email' :
            body = "Check out HTC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/htc";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'kyo_email' :
            body = "Check out Kyocera products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/kyocera";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'asus_email' :
            body = "Check out Asus® products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/asus-computer";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'lg_email' :
            body = "Check out LG products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/lg";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'moto_email' :
            body = "Check out Motorola products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/motorola";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'moti_email' :
            body = "Check out Motion Computing  products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/motion-computing";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'nec_email' :
            body = "Check out NEC products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/nec";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'nokia_email' :
            body = "Check out Nokia products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/nokia";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'sam_email' :
            body = "Check out Samsung products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/samsung";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'sk_email' :
            body = "Check out SK Telesys products featuring CorningA® GorillaA® Glass at http://www.corninggorillaglass.com/products-with-gorilla/sk-telesys";
            subject = "Corning® Gorilla® Glass";
        break;  
        
        case 'sony_email' :
            body = "Check out Sony products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/SonyBRAVIA/for_bravia.html";
            subject = "Corning® Gorilla® Glass";
        break;

        case 'lenovo_email' :
            body = "Check out Lenovo products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/lenovo";
            subject = "Corning® Gorilla® Glass";
        break;
        
        case 'hyundai_email' :
            body = "Check out Hyundai products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/hyundai";
            subject = "Corning® Gorilla® Glass";
        break;     
          
          
        case 'praise_email':
            body = "Want to show your praise for Corning® Gorilla® Glass? Send us your feedback at http://www.corninggorillaglass.com/praise-for-gorilla";
            subject = "Corning® Gorilla® Glass";
        break;

        case 'allpraise_email':
            body = "Want to show your praise for Corning® Gorilla® Glass? Send us your feedback at http://www.corninggorillaglass.com/praise-for-gorilla";
            subject = "Corning® Gorilla® Glass";
        break;  
          
        case 'sur40_email':
            body = "Check out Samsung products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/samsungsur40";
            subject = "Corning® Gorilla® Glass";
        break; 
        
    	case 'hp_email':
            body = "Check out HP products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/hp";
            subject = "Corning® Gorilla® Glass";
        break; 
          
        case 'sonim_email':
            body = "Check out Sonim products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/products-with-gorilla/sonim";
            subject = "Corning® Gorilla® Glass";
        break; 
        
        case 'lumigon_email':
            body = "Check out Lumigon products featuring Corning® Gorilla® Glass at http://www.corninggorillaglass.com/product/lumigon";
            subject = "Corning® Gorilla® Glass";
    	break;
        
    }    
    setTimeout(function(){
    	//open email client
    	location.href = 'mailto:?'+'subject='+subject+'&body='+body;
    }, 500);

};






//------------------------ SMS --------------------------------
gorilla.clearSmslabel = function()
{
	//clear textfields and labels
	document.getElementById('sms_name').value = "";
    document.getElementById('sms_phone').value = "";
	
    document.getElementById('validation_sms').innerHTML = "";
}

gorilla.shareViaSMS = function()
{         	       
    //get values
    var sms_name = document.getElementById('sms_name').value;
    var sms_pre_phone = document.getElementById('sms_phone').value;
	var sms_count = sms_pre_phone.length;
    var msgId = "";
    
    if (sms_pre_phone == "" || sms_count < 10 || sms_count > 10 ) 
    {
    	document.getElementById('validation_sms').innerHTML = "Please insert a 10-digit phone number.";
    }
    else
    {
		var sms_phone = "+1" + sms_pre_phone;	
		
		//alert(selectedVideoToShare);
		
        switch (gorilla.smsButtonId)
        {
            case 'home_sms' :
                msgId = "1";
            break;
            
            case 'products_sms' :
                msgId = "2";
            break;
            
            case 'fun_sms' :
                msgId = "26";
            break;
        
            case 'faq_sms' :
                msgId = "39";
            break;
        
            case 'productsfull_sms' :
                msgId = "3";
            break;  
        
            case 'news_sms' :
                msgId = "19";
            break; 
            
            case 'specificnews_sms' :
                msgId = "20";
            break;
            
            //innovating
            case 'over_sms' ://OVERVIEW
                msgId = "21";
            break;
            
            case 'chara_sms' ://CHARACTERISTICS
                msgId = "22";
            break;
            
            case 'custo_sms' ://CUSTOMIZATION            	            	
                msgId = "23";            
            break;
            
            case 'appli_sms' ://APPLICATIONS
                msgId = "24";
            break;  

            case 'lite_sms' ://LITERATURE
                msgId = "25";
            break;  
            
            
            //videos        
            case 'victor_sms' :
                if (selectedVideoToShare === 0)//channel surfing 
                { 
                    msgId = "32";
                    //analytics
                    veltijs.pushCustomEventAnalytics('victor1_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "Channel Surfing video", "Videos page"];
                    //fiveml._invokeTracking(args);
                }
                else if (selectedVideoToShare === 1)//cooking up tomorrow's kitchen
                { 
                    msgId = "33";
                    //analytics
                    veltijs.pushCustomEventAnalytics('victor2_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "Cooking Up Tomorrow's Kitchen video", "Videos page"];
                    //fiveml._invokeTracking(args);
                }
                else if (selectedVideoToShare === 2)//king of the office
                { 
                    msgId = "34";
                    //analytics
                    veltijs.pushCustomEventAnalytics('victor3_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "King of the Office video", "Videos page"];
                    //fiveml._invokeTracking(args);
                }
            break;
            case 'demos_sms' :
                if (selectedVideoToShare === 0)// How Corning Tests Gorilla Glass
                {                	
                    msgId = "102";
                    //analytics
                    veltijs.pushCustomEventAnalytics('demos1_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "How Corning Tests Gorilla Glass", "Videos page" ];
                    //fiveml._invokeTracking(args);
                }
            break;
            case 'saying_sms' ://gorilla tough
                if (selectedVideoToShare === 0)// gorilla tough
                {
                    msgId = "37";
                    //analytics
                    veltijs.pushCustomEventAnalytics('saying1_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "Gorilla Tough video", "Videos page"];
                    //fiveml._invokeTracking(args);              
                }
                else if (selectedVideoToShare === 1)// Microsoft Puts Corning Gorilla Glass 2 to the Test
                {
                    msgId = "102";
                    // analytics
                    veltijs.pushCustomEventAnalytics('saying2_sms');
                    
                    //var args = [ "trackEvent", "GorillaVideos", "SmsShares", "Microsoft Puts Corning Gorilla Glass 2 to the Test", "Videos page" ];
                    //fiveml._invokeTracking(args);
                }
            break;
            case 'brought_sms' ://a day made of
                if (selectedVideoToShare === 0)// a day made of
                {
                    msgId = "38";
                    //analytics
                    veltijs.pushCustomEventAnalytics('brought1_sms');
                    
                    //var args = ["trackEvent", "GorillaVideos", "SmsShares", "A Day Made of Glass video", "Videos page"];
                    //fiveml._invokeTracking(args);
                }
                else if (selectedVideoToShare === 1)// Corning Gorilla Glass for Seamless Elegant Design
                {
                    msgId = "102";
                    // analytics
                    veltijs.pushCustomEventAnalytics('brought2_sms');
                    
                    //var args = [ "trackEvent", "GorillaVideos", "SmsShares", "Corning Gorilla Glass for Seamless Elegant Design", "Videos page" ];
                    //fiveml._invokeTracking(args);
                }
                else if (selectedVideoToShare === 2)// Changing the Way We Think about Glass
                {
                    msgId = "141";
                    // analytics
                    veltijs.pushCustomEventAnalytics('brought3_sms');
                    
                    //var args = [ "trackEvent", "GorillaVideos", "SmsShares", "Changing the Way We Think about Glass", "Videos page"];
                    //fiveml._invokeTracking(args);
                }
            break;  
            
            
            //wallpapers
            case 'wallpaper_sms' :
                if (currentId == 'coverflow0pic1' )//fatsa
                {
                    msgId = "30";
                    
                    //analytics
                    veltijs.pushCustomEventAnalytics('coverflow0pic1_sms');
                    
                    //var args = ["trackEvent", "SpecificWallpaper", "SmsShares", "Wallpaper - Gorilla face", "Wallpaper - Gorilla face"];
                    //fiveml._invokeTracking(args);
                }else if(currentId == 'coverflow0pic2')//filaei kato
                {
                    msgId = "29";
                    
                    //analytics
                    veltijs.pushCustomEventAnalytics('coverflow0pic2_sms');
                    
                    //var args = ["trackEvent", "SpecificWallpaper", "SmsShares", "Wallpaper - Gorilla Kiss", "Wallpaper - Gorilla Kiss"];
                    //fiveml._invokeTracking(args);
                }else if(currentId == 'coverflow0pic3')//kornizes
                {
                    msgId = "27";
                    
                    //analytics
                    veltijs.pushCustomEventAnalytics('coverflow0pic3_sms');
                    
                    //var args = ["trackEvent", "SpecificWallpaper", "SmsShares", "Wallpaper - Gorilla Frame", "Wallpaper - Gorilla Frame"];
                    //fiveml._invokeTracking(args);
                }else if(currentId == 'coverflow0pic4')//bananes
                {
                    msgId = "28";
                    
                    //analytics
                    veltijs.pushCustomEventAnalytics('coverflow0pic4_sms');
                    
                    //var args = ["trackEvent", "SpecificWallpaper", "SmsShares", "Wallpaper - Gorilla Banana", "Wallpaper - Gorilla Banana"];
                    //fiveml._invokeTracking(args);
                }else if(currentId == 'coverflow0pic5')//Fossey
                {
                    msgId = "101";
                    
                    //analytics
                    veltijs.pushCustomEventAnalytics('coverflow0pic5_sms');
                    
                    //var args = ["trackEvent", "SpecificWallpaper", "SmsShares", "Wallpaper - Gorilla Fossey", "Wallpaper - Gorilla Fossey"];
                    //fiveml._invokeTracking(args);
                }
            break;


            //products
            case 'acer_sms' :
                msgId = "4";
            break;  
            
            case 'dell_sms' :
                msgId = "6";
            break;  
            
            case 'htc_sms' :
                msgId = "7";
            break;  
            
            case 'kyo_sms' :
                msgId = "8";
            break;  
            
            case 'asus_sms' :
                msgId = "5";
            break;  
            
            case 'lg_sms' :
                msgId = "10";
            break;  
            
            case 'moto_sms' :
                msgId = "11";
            break;  
            
            case 'moti_sms' :
                msgId = "12";
            break;  
            
            case 'nec_sms' :
                msgId = "13";
            break;  
            
            case 'nokia_sms' :
                msgId = "14";
            break;  
            
            case 'sam_sms' :
                msgId = "15";
            break;  
            
            case 'sk_sms' :
                msgId = "16";
            break;  
            
            case 'sony_sms' :
                msgId = "17";
            break;  

            case 'lenovo_sms' :
                msgId = "9";
            break;  

            case 'hyundai_sms' :
                msgId = "18";
            break;
                          
            case 'praise_sms':
            	msgId = "81";
            break;
        
            case 'allpraise_sms':
            	msgId = "81";
            break;
                
            case 'sur40_sms':
            	msgId = "62";
            break;
            
            case 'hp_sms':
            	msgId = "61";
            break;  
              
            case 'sonim_sms':
            	msgId = "82";
            break;
            
            case 'lumigon_sms':
            	msgId = "142";
            break;
              
        }
        
	var jsonObj = {"clientId":"corning_gorilla","msgId":msgId,"msisdns":[sms_phone],"params":{"name":sms_name}};
		
        
	var headers =   {
		  'Content-Type' : veltijs.proxy.TYPE_JSON,
		  'Accept' : 'application/json'
	};
	
	veltijs.proxy.sendRequest(gorilla.services.sms_service, 'POST', true, jsonObj, gorilla.responseShareViaSMS, headers);	
      
    }
};

gorilla.cancelSms = function()
{
	//alert(gorilla.smsPrevPage);
	gorilla.showPageById(gorilla.smsPrevPage);
}


gorilla.responseShareViaSMS = function(req)
{       
	var response = req.responseText;	
	var sms_validation_msg = document.getElementById('validation_sms');
   
	//alert(response.search("ORDERID"));
	if (response.search("ORDERID") != -1) {
		sms_validation_msg.innerHTML="Thank you. This message has been successfully sent to your mobile.";
	}
	else{
		sms_validation_msg.innerHTML="I’m sorry, please try again.";
	}      
};



gorilla.addScroll = function (id){
	//setTimeout(function(){
	console.log("running scroller");
	var my_mywrapper = document.getElementById(id)
	var scroller = my_mywrapper.children[0];//document.getElementById("scroller");
	var thelist = scroller.children[0];//document.getElementById("thelist");
	var itemslength = thelist.children.length;
	scroller.style.width = (itemslength *140) +'px';
	var myScroll = new iScroll(id,{hScrollbar:false });
	gorilla.currentScroller = myScroll;
	//},100);
};


//---------------------------- Gorilla FunWithGorilla Page Carouzel -----------------------------

gorilla.flow= function(params)
{
  
	var cont = params[0];
	var imageWidth = parseInt(Math.round(parseInt(cont.style.width)/3));
	var scroller = cont.children[0]
	var absPos = Math.abs(cont.position);
	var pics = scroller.children;	
	var picslength = pics.length -2;
	var android = ((navigator.userAgent.toLowerCase().search('android') > -1)|| (navigator.userAgent.toLowerCase().search("kftt")>-1))?true:false;
	for(var d=0;d<picslength;d++)
	{
		var pic = pics[absPos+d];		
		if(d==0)
		{
			
			if ((absPos)>d+1)
			{
				pics[d+1].style.opacity = 0.0;
			}			
			pic.style.opacity = 0.5;
			if(!android)
				pic.style.webkitTransform ='scaleY(1.2) rotateY(30deg)';	
		}
		else if(d==1)
		{
			if ((absPos)>d+1)
			{
				pics[d+1].style.opacity = 0.0;
			}
			pic.style.opacity = 1.0;
			if(!android)
				pic.style.webkitTransform ='scale(1.7) translateZ(1px)';
		}
		else if(d==2)
		{
			if ((absPos)>d+1)
			{
				pics[d+1].style.opacity = 0.0;
			}
			pic.style.opacity = 0.5;
			if(!android)
				pic.style.webkitTransform ='scaleY(1.2) rotateY(-30deg)';
		}
		else if ((absPos+d) <6) 
		{					
			pic.style.opacity = 0.0;	
		}			
		
	}
	if(android)
    {
		scroller.style.zoom = 1.6;
        scroller.style.webkitTransform = 'translate3d('+(cont.position*imageWidth-63)+'px,-50px,0px)';
    }
    else
    {	        //console.log(cont.position*imageWidth);
    	scroller.style.webkitTransform ='translateX('+(cont.position*imageWidth)+'px)';
	}

}

gorilla.preloadFunWallpapers = function()
{
	
	gorilla.FunWallpapers = [ "http://meds.mblgrt.com/ch/29645/Corning_GorillaGlass_Wall?ni&d=raw&uaprof="+USER_AGENT,	                 	
	                 	"http://meds.mblgrt.com/ch/29621/Corning_GorillaGlass_Wall?ni&d=raw&uaprof="+USER_AGENT,
	                 	"http://meds.mblgrt.com/ch/29629/Corning_GorillaGlass_Wall?ni&d=raw&uaprof="+USER_AGENT,
	                 	"http://meds.mblgrt.com/ch/29637/Corning_GorillaGlass_Wall?ni&d=raw&uaprof="+USER_AGENT,
	                 	"http://meds.mblgrt.com/ch/36463/Fossy_Wallpapers?ni&d=raw&uaprof="+USER_AGENT
	                 ]
	gorilla.FunWallpapersd = []
	for(var i=0;i<gorilla.FunWallpapers.length;i++)
	{
		//alert(gorilla.FunWallpapers[i]);
		var img = new Image;
		img.style.width="100%"
		img.src = gorilla.FunWallpapers[i];
		gorilla.FunWallpapersd.push(img);
	}

}

gorilla.FuncoverflowClickEvent = function(e)
{

	currentPic = e.src;
    currentId = e.id;
    var container = e.parentNode.parentNode;  
    var theImg=gorilla.FunWallpapers[0];
    var imgCont = document.getElementById("wallpaperImg");
    if(imgCont.children.length >0){
    	imgCont.removeChild(imgCont.children[0]);
    }
    if(container.noAnim)
    {
    	if (currentId == 'coverflow0pic1' )
        {
    		theImg = gorilla.FunWallpapersd[0];                	  
      	}
        else if(currentId == 'coverflow0pic2')
        {
        	theImg = gorilla.FunWallpapersd[1];
 		}
        else if(currentId == 'coverflow0pic3')
        {
        	theImg = gorilla.FunWallpapersd[2];
        }
        else if(currentId == 'coverflow0pic4')
        {
        	theImg = gorilla.FunWallpapersd[3];
        }
        else if(currentId == 'coverflow0pic5')
        {
        	theImg = gorilla.FunWallpapersd[4];
        }
      
    imgCont.appendChild(theImg);
    gorilla.showPageById("WallpaperPreview_page"); 
    }

}


gorilla.coverflow = function(k)
{
	var imageWidth = parseInt(Math.round(parseInt(k.style.width)/3));
 	var scroller = k.children[0];
	var pics = scroller.children;
    k.position = -1;	   
    k.style.overflow='hidden';
    var width = parseInt(k.style.width);

    var sample = new Image();
    sample.src = pics[1].src;
    sample.onload = function()
    {
        scroller.style.cssText = '-webkit-transition:-webkit-transform .4s ease-in-out;PADDING-TOP:54px;width:'+((pics.length+2)*imageWidth)+'px;HEIGHT:'+(sample.height-150)+'px;-webkit-transform:translate('+ (k.position*parseInt(width/3))+'px,0px)';
    	scroller.style.webkitPerspective = 700;	
	
		veltijs.addSwipe(k.id,
				 function(div)
			      {
			          
			 		var cont = div.parentNode.parentNode;
			 		if (++cont.position === 1)
				        {
			 			 	cont.position = 0;
				            return;
				        }
			 		
			    	gorilla.flow([cont]);
			      	
			      },
			      function(div)
			      {
			    	  var cont = div.parentNode.parentNode;
			    	  var picsLength = div.parentNode.children.length - 2			    	 
			    	  if (--cont.position === (-1*picsLength))
			          {
			    		
			    		  cont.position = (picsLength-1)*-1
			              return;
			          }			    	
			          gorilla.flow([cont]);
			          
			      });
		
        var coverflow0pic0 = pics[0];
        coverflow0pic0.id = 'coverflow0pic0';
        coverflow0pic0.style.cssText = 'float:left;width:'+imageWidth+'px;HEIGHT:1px';     
		
		var picslength = pics.length -2;
        for(var p=0;p<picslength;p++)
        {
            var piccie = pics[p+1];
			piccie.id = 'coverflow0pic'+(parseInt(p)+1);
          
            piccie.style.cssText = 'float:left;width:'+imageWidth+'px;-webkit-transition:'+((navigator.userAgent.toLowerCase().search('android') > -1 || (navigator.userAgent.toLowerCase().search("kftt")>-1)) ? '-webkit-transform' : 'all')+' .3s ease-in-out';
        }
        var coverflow0picEnd = pics[pics.length -1];
        coverflow0picEnd.id = 'coverflow0pic'+(pics.length-1);
        coverflow0picEnd.style.cssText = 'float:left;width:'+imageWidth+'px;HEIGHT:1px';
        setTimeout(function(){gorilla.flow([k]);}, 1000);
    };
}

//----------------------------- Praise


